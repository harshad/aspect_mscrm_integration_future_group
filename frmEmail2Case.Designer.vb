﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmail2Case
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmail2Case))
        Me.wbEmailToCase = New System.Windows.Forms.WebBrowser()
        Me.tmrEmail2Case = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'wbEmailToCase
        '
        Me.wbEmailToCase.Location = New System.Drawing.Point(42, 40)
        Me.wbEmailToCase.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.wbEmailToCase.MinimumSize = New System.Drawing.Size(15, 16)
        Me.wbEmailToCase.Name = "wbEmailToCase"
        Me.wbEmailToCase.Size = New System.Drawing.Size(167, 181)
        Me.wbEmailToCase.TabIndex = 8
        '
        'tmrEmail2Case
        '
        '
        'frmEmail2Case
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1904, 1041)
        Me.Controls.Add(Me.wbEmailToCase)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmEmail2Case"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmEmail2Case"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents wbEmailToCase As WebBrowser
    Private WithEvents tmrEmail2Case As Timer
End Class
