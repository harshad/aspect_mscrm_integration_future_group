Public Class Form2

    Dim TransferNumber As String

    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TransferNumber = ""
        TextBoxTranferNumber.Text = ""
        TextBoxLineNum.Text = ""
    End Sub

    Public Function GetTransferNumber() As String
        Return TransferNumber
    End Function

    Public Function GetLineNum() As Integer
        Dim num As Integer = 0
        If TextBoxLineNum.Text.Length <= 0 Then
            Return 0
        End If

        Return CInt(TextBoxLineNum.Text)

    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If TextBoxTranferNumber.Text.Length > 0 Then
            TransferNumber = TextBoxTranferNumber.Text
            TextBoxTranferNumber.Text = ""

            DialogResult = DialogResult.OK
        Else
            MsgBox("Please enter the transfer number")
        End If
    End Sub

End Class