﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("CSISIP")> 
<Assembly: AssemblyDescription("RegisterFailure,OutofService,LackofAudioDevice problem resolved already and released on 12th Apr 2012[1.0.1.0]/Ringing FIle Removed as per mandy on 13th Apr 2012[1.0.1.1]/SessionTimer Added,StackTrace Added,session time added in config,RTPKeepAlive added and in config on 20thApril 2012[1.0.1.2]/Release on 21th April 2012[1.0.1.3]")> 
<Assembly: AssemblyCompany("CS Infocomm Pvt. Ltd.")> 
<Assembly: AssemblyProduct("CSISIP")> 
<Assembly: AssemblyCopyright("Copyright ©  2012")> 
<Assembly: AssemblyTrademark("® Manas Kar")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f3227f9c-7082-4c6d-ae7b-eb12b5a8b529")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.1.5")>
<Assembly: AssemblyFileVersion("1.0.1.5")>

<Assembly: NeutralResourcesLanguageAttribute("en")> 