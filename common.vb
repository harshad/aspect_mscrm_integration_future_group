Imports System.Runtime.InteropServices
Imports System.IO
Module common
    Private Declare Function IsNetworkAlive Lib "SENSAPI.DLL" (ByRef lpdwFlags As Long) As Long
    Const NETWORK_ALIVE_AOL = &H4
    Const NETWORK_ALIVE_LAN = &H1
    Const NETWORK_ALIVE_WAN = &H2
    <StructLayout(LayoutKind.Sequential)> _
    Public Structure QOCINFO
        Public dwSize As UInteger
        Public dwFlags As UInteger
        Public dwInSpeed As UInteger
        Public dwOutSpeed As UInteger
    End Structure
    <DllImport("SensApi.dll", SetLastError:=True)> _
    Public Function IsDestinationReachable(ByVal lpszDestination As String, ByRef lpQOCInfo As QOCINFO) As Boolean
    End Function
    Public NotInheritable Class NativeMethods
        '
        ' NativeMethods
        '
        ' Constructor. Accessibility is set to private to prevent the creation of
        ' an instance of the class, because all public members are static.
        '
        Public Sub New()
        End Sub

        '
        ' IsNetworkAlive
        '
        ' Determines whether or not a local system is connected to a network,
        ' and identifies the type of network connection, for example, a LAN, WAN, or both.
        '
        ' Parameters:
        ' lpdwFlags - [out] The type of network connection that is available.
        ' This parameter is set to NetworkConnectionType.
        '
        <DllImport("SensApi.dll", SetLastError:=True)> _
        Public Shared Function IsNetworkAlive(ByRef lpdwFlags As UInteger) As Boolean
        End Function

        '
        ' IsDestinationReachable
        '
        ' Determines whether or not a specified destination can be reached,
        ' and provides Quality of Connection (QOC) information for a destination.
        '
        ' Parameters:
        ' lpszDestination - [in] A pointer to a null-terminated string that specifies a destination.
        ' The destination can be an IP address, UNC name, or URL.
        ' lpQOCInfo - [in, out] A pointer to the QOCINFO structure that receives the Quality of Connection (QOC) information.
        ' Supply a NULL pointer if you do not want to receive the QOC information.
        '
        <DllImport("SensApi.dll", SetLastError:=True)> _
        Public Shared Function IsDestinationReachable(ByVal lpszDestination As String, ByRef lpQOCInfo As QOCINFO) As Boolean
        End Function

        '
        ' GetSystemPowerStatus
        '
        ' Retrieves the power status of the system. The status indicates whether the system is running
        ' on AC or DC power, whether the battery is currently charging, and how much battery life remains.
        '
        ' Parameters:
        ' lpSystemPowerStatus - [out] Pointer to a SYSTEM_POWER_STATUS structure that receives status information.
        '
        ''<DllImport("Kernel32.dll", SetLastError:=True)> _
        ''Public Shared Function GetSystemPowerStatus(ByRef lpSystemPowerStatus As SYSTEM_POWER_STATUS) As Boolean
        ''End Function
    End Class
    Public Function IsHostReachable(ByVal hostaddress As String) As Boolean
        Dim qocinfo As New QOCINFO()
        qocinfo.dwSize = CUInt(Marshal.SizeOf(GetType(QOCINFO)))
        Return NativeMethods.IsDestinationReachable(hostaddress, qocinfo)
    End Function
    Private Declare Function InternetGetConnectedState Lib "wininet.dll" (ByRef _
        lpSFlags As Int32, ByVal dwReserved As Int32) As Boolean
    'Private Declare Function InternetGetConnectedState Lib "SensApi.dll" (ByRef _
    '        lpSFlags As Int32, ByVal dwReserved As Int32) As Boolean

    Public Enum InetConnState
        modem = &H1
        lan = &H2
        proxy = &H4
        ras = &H10
        offline = &H20
        configured = &H40
    End Enum

    Public Function NetworkConnection() As Boolean
        ' ''Dim lngFlags As Long
        '' ''Dim ConnectionStateString As String
        ' ''If InternetGetConnectedState(lngFlags, 0) Then
        ' ''    Return True
        ' ''    ' True
        ' ''    'If lngFlags And InetConnState.lan Then
        ' ''    '    ConnectionStateString = "LAN."
        ' ''    'ElseIf lngFlags And InetConnState.modem Then
        ' ''    '    ConnectionStateString = "Modem."
        ' ''    'ElseIf lngFlags And InetConnState.configured Then
        ' ''    '    ConnectionStateString = "Configured."
        ' ''    'ElseIf lngFlags And InetConnState.proxy Then
        ' ''    '    ConnectionStateString = "Proxy"
        ' ''    'ElseIf lngFlags And InetConnState.ras Then
        ' ''    '    ConnectionStateString = "RAS."
        ' ''    'ElseIf lngFlags And InetConnState.offline Then
        ' ''    '    ConnectionStateString = "Offline."
        ' ''    'End If
        ' ''Else
        ' ''    Return False
        ' ''    ' False
        ' ''    'ConnectionStateString = "Not Connected."
        ' ''End If
        '' ''If IsHostReachable("97.253.42.55") = False Then
        '' ''    Return False
        '' ''Else
        '' ''    Return True
        '' ''End If
        Dim ChkNetwork As Long
        If IsNetworkAlive(ChkNetwork) = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Public username As String
    Public Sub LogMaintain(ByVal str As String)
        Dim e As String
        e = "Activity - " & str
        On Error Resume Next
        Dim action As String
        action = Date.Now.ToString("dd-MM-yyyy hh:mm:ss:fff tt")
        Dim evalue As String
        evalue = "Time - " & action & " ; " & e
        Dim epath As String
        epath = Application.StartupPath & "\Logs\" & Date.Now.ToString("dd-MM-yyyy") & ".txt"
        If Directory.Exists(Application.StartupPath & "\Logs") = False Then
            Directory.CreateDirectory(Application.StartupPath & "\Logs")
        End If
        Dim sw As StreamWriter
        If File.Exists(epath) = False Then
            sw = File.CreateText(epath)
            sw.WriteLine(evalue)
            sw.Close()
        Else
            sw = File.AppendText(epath)
            sw.WriteLine(evalue)
            sw.Close()
        End If
    End Sub
    Public Sub log_SipMsg(ByVal str As String)
        Dim e As String
        e = str
        On Error Resume Next
        Dim action As String
        action = Date.Now.ToString("dd-MM-yyyy hh:mm:ss:fff tt")
        Dim evalue As String
        evalue = action & "-" & e
        Dim epath As String
        epath = Application.StartupPath & "\Logs\MSG" & Date.Now.ToString("dd-MM-yyyy") & ".txt"
        If Directory.Exists(Application.StartupPath & "\Logs") = False Then
            Directory.CreateDirectory(Application.StartupPath & "\Logs")
        End If
        Dim sw As StreamWriter
        If File.Exists(epath) = False Then
            sw = File.CreateText(epath)
            sw.WriteLine(evalue)
            sw.Close()
        Else
            sw = File.AppendText(epath)
            sw.WriteLine(evalue)
            sw.Close()
        End If
    End Sub
End Module
