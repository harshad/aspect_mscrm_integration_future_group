Public Class DisplayWindow

    Public Declare Function apiFindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Integer
    Public Declare Function apiGetTopWindow Lib "user32" Alias "GetTopWindow" (ByVal hwnd As Integer) As Integer
    Public Declare Function apiGetDesktopWindow Lib "user32" Alias "GetDesktopWindow" () As Integer
    Dim rTopMost As New System.Threading.Thread(AddressOf ReturnTopMost) 'Declare thread for returning topmost

    Delegate Function HandleDelegate() As Int32
    Public hndl As HandleDelegate

    Delegate Sub setTopMostDelegate()
    Public setTopMost As setTopMostDelegate

    Delegate Sub setRefreshDelegate()
    Public setRefresh As setRefreshDelegate

    Delegate Function getTextDelegate() As String
    Public getText As getTextDelegate

    Public Sub setTopMostSub()
        Me.TopMost = True
    End Sub

    Public Sub setRefreshSub()
        Me.Refresh()
    End Sub

    Public Function getHandleFunc() As Int32
        Return Me.Handle.ToInt32
    End Function

    Public Function getTextSub() As String
        Return Me.Text.ToString
    End Function

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Left = Form1.displaywindowleft
        Me.Top = Form1.displaywindowtop
        Me.Width = Form1.displaywindowwidth
        Me.Height = Form1.displaywindowheight
        BigVideo.Left = 0
        BigVideo.Top = 0
        BigVideo.Width = Me.Width
        BigVideo.Height = Me.Height
        SmallVideo.Width = Me.Width / 4
        SmallVideo.Height = Me.Height / 3
        SmallVideo.Left = Me.Width - (Me.Width / 4)
        SmallVideo.Top = Me.Height - (Me.Height / 3)

        hndl = New HandleDelegate(AddressOf getHandleFunc)
        setTopMost = New setTopMostDelegate(AddressOf setTopMostSub)
        setRefresh = New setRefreshDelegate(AddressOf setRefreshSub)
        getText = New getTextDelegate(AddressOf getTextSub)

        rTopMost.Start() 'Start Return Top Most thread.
    End Sub

    Private Sub Form1_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        rTopMost.Abort() 'Abort TopMost thread if the main form is closed 
    End Sub

    Public Sub ReturnTopMost()

        Dim iHndl As Int32

        iHndl = Me.Invoke(hndl)

        Try
            'Do
            System.Threading.Thread.Sleep(1) 'Sleep for a short period
            If apiGetTopWindow(apiGetDesktopWindow) <> iHndl Then
                Me.Invoke(setTopMost)
                Me.Invoke(setRefresh)
            End If
            apiFindWindow(vbNullString, Me.Invoke(getText))  'This throws an error if the main window has been terminated.
            'Loop
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    
    Private Sub BigVideo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BigVideo.Click

    End Sub
End Class