Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports CSISIP.cTripleDES
Imports System.Text
Imports UADConnector

Public Class Form1

    'close button disable
    'Protected Overrides ReadOnly Property CreateParams() As CreateParams
    '    Get
    '        Dim cp As CreateParams = MyBase.CreateParams
    '        Const CS_NOCLOSE As Integer = &H200
    '        cp.ClassStyle = cp.ClassStyle Or CS_NOCLOSE
    '        Return cp
    '    End Get
    'End Property
    Dim serverSocket As TcpListener
    Dim clientSocket As TcpClient
    Dim SocketThread As System.Threading.Thread
    Dim serverSocket2 As TcpListener
    Dim clientSocket2 As TcpClient
    Dim SocketThread2 As System.Threading.Thread

    Private SIPInited As Boolean
    Private SIPLogined As Boolean

    Private CurrentlyLine As Long
    Private CallSessions(9) As Sessions ' 8 lines

    Private Const MAX_LINES As Long = 4 ' Maximum lines
    Private Const LINE_BASE As Long = 1

    ' Audio codecs 
    Private Const AUDIOCODEC_PCMU As Long = 0
    Private Const AUDIOCODEC_GSM As Long = 3
    Private Const AUDIOCODEC_G723 As Long = 4 ' G723.1
    Private Const AUDIOCODEC_PCMA As Long = 8
    Private Const AUDIOCODEC_G729 As Long = 18
    Private Const AUDIOCODEC_G722 As Long = 9 'bitrate 64000bps, 16KHZ
    Private Const AUDIOCODEC_ILBC As Long = 97
    Private Const AUDIOCODEC_AMRWB As Long = 98 'bitrate 12650bps
    Private Const AUDIOCODEC_SPEEX As Long = 99 'bitrate 8000bps
    Private Const AUDIOCODEC_SPEEXWB As Long = 100 'bitrate 8000bps
    Private Const AUDIOCODEC_G7221 As Long = 121 'bitrate 24000bps
    Private Const AUDIOCODEC_DTMF As Long = 101


    ' Video codecs
    Private Const VIDEOCODEC_H263 As Long = 34
    Private Const VIDEOCODEC_H263_1998 As Long = 115
    Private Const VIDEOCODEC_H264 As Long = 125



    ' Video resolution
    Private Const VIDEO_QCIF As Long = 1  ' 176X144     - for H263, H263-1998, H264
    Private Const VIDEO_CIF As Long = 2   ' 352X288     - for H263, H263-1998, H264
    Private Const VIDEO_VGA As Long = 3   ' 640X480     - for H264 only
    Private Const VIDEO_SVGA As Long = 4  ' 800X600		- for H264 only
    Private Const VIDEO_XVGA As Long = 5  ' 1024X768	- for H264 only
    Private Const VIDEO_720P As Long = 6  ' 1280X720	- for H264 only

    ' Video quality
    Private Const VIDEO_QUALITY_LEVEL1 As Long = 1 ' Best
    Private Const VIDEO_QUALITY_LEVEL2 As Long = 2
    Private Const VIDEO_QUALITY_LEVEL3 As Long = 3
    Private Const VIDEO_QUALITY_LEVEL4 As Long = 4
    Private Const VIDEO_QUALITY_LEVEL5 As Long = 5
    Private Const VIDEO_QUALITY_LEVEL6 As Long = 6 ' bad


    ' Audio record file format
    Private Const FILEFORMAT_WAVE As Long = 1
    Private Const FILEFORMAT_OGG As Long = 2
    Private Const FILEFORMAT_MP3 As Long = 3


    ' Video recording mode
    Private Const VIDEOINFO_ONLY_LOCAL = 1   ' Record local video only
    Private Const VIDEOINFO_ONLY_REMOTE = 2     ' Record remote video only
    Private Const VIDEOINFO_BOTH = 3            ' Record both of local and remote video


    ' Audio callback mode
    Private Const AUDIOSTREAM_NONE = 0      ' Disable audio stream callback
    Private Const AUDIOSTREAM_LOCAL = 1     '   Enable audio stream callback for local audio stream
    Private Const AUDIOSTREAM_REMOTE = 2    ' Enable audio stream callback for remote audio stream
    Private Const AUDIOSTREAM_LOCAL_REMOTE_MIX = 3      ' The mixed stream of local sound card captured audio and received remote audio  
    Private Const AUDIOSTREAM_LOCAL_REMOTE_SEPARATE = 4      ' Local sound card captured audio stream and received remote audio stream 



    ' Access video stream callback mode
    Private Const VIDEOSTREAM_NONE = 0 ' Disable video stream callback
    Private Const VIDEOSTREAM_LOCAL = 1 ' Local video stream callback
    Private Const VIDEOSTREAM_REMOTE = 2 ' Remote video stream callback
    Private Const VIDEOSTREAM_BOTH = 3 ' Both of local and remote video stream callback



    ' Log level
    Private Const PORTSIP_LOG_NONE = -1
    Private Const PORTSIP_LOG_DEBUG = 0
    Private Const PORTSIP_LOG_ERROR = 1
    Private Const PORTSIP_LOG_WARNING = 2
    Private Const PORTSIP_LOG_INFO = 3


    ' SRTP policy
    Private Const SRTP_POLICY_NONE = 0
    Private Const SRTP_POLICY_FORCE = 1
    Private Const SRTP_POLICY_PREFER = 2


    ' Transport
    Private Const TRANSPORT_UDP = 0
    Private Const TRANSPORT_TLS = 1
    Private Const TRANSPORT_TCP = 2

    Private Sub InitDefaultAudioCodecs()
        If SIPInited = False Then
            Exit Sub
        End If

        AxPortSIPCoreLib1.clearAudioCodec()

        ' Default we just using PCMU, G729 ,G722.1
        AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_PCMU) ' for PCMU
        AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_G729) ' for G729
        AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_G7221) ' for G722.1

        ' AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_ILBC) ' for iLBC
        'AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_GSM) ' for GSM
        ' AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_G723) ' G723.1


        AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_DTMF) ' for DTMF - RFC 2833


        ' To enable DTMF 2833
        AxPortSIPCoreLib1.enableDtmfOfRFC2833(101)

        ' If want to use SIP INFO METHOND, then
        ' AxPortSIPCoreLib1.enableDTMFOfInfo();
    End Sub

    Private Sub InitSettings()
        If SIPInited = False Then
            Exit Sub
        End If


        AxPortSIPCoreLib1.enableDtmfOfRFC2833(101)

        AxPortSIPCoreLib1.setDtmfSamples(160)

        ' enable AEC, if want to disable AEC, use AxPortSIPCoreLib1.enableAEC(0)
        If CheckBoxAEC.Checked = True Then
            AxPortSIPCoreLib1.enableAEC(1)
        Else
            AxPortSIPCoreLib1.enableAEC(0)
        End If

        If CheckBoxVAD.Checked = True Then
            AxPortSIPCoreLib1.enableVAD(1)
        Else
            AxPortSIPCoreLib1.enableVAD(0)
        End If

        If CheckBoxCNG.Checked = True Then
            AxPortSIPCoreLib1.enableCNG(1)
        Else
            AxPortSIPCoreLib1.enableCNG(0)
        End If

        If CheckBoxAGC.Checked = True Then
            AxPortSIPCoreLib1.enableAGC(1)
        Else
            AxPortSIPCoreLib1.enableAGC(0)
        End If

    End Sub

    Private Sub SetSRTPType()
        If SIPInited = False Then
            Exit Sub
        End If

        Dim SRTPType As Integer = SRTP_POLICY_PREFER

        If ComboBoxSRTP.SelectedIndex = 0 Then
            SRTPType = SRTP_POLICY_NONE
        ElseIf ComboBoxSRTP.SelectedIndex = 1 Then
            SRTPType = SRTP_POLICY_PREFER
        Else
            SRTPType = SRTP_POLICY_FORCE
        End If
        AxPortSIPCoreLib1.setSrtpPolicy(SRTPType)
    End Sub

    Private Sub SetVideoResolution()
        If SIPInited = False Then
            Exit Sub
        End If

        Dim videoResolution As Integer = VIDEO_QCIF
        Select Case ComboBoxVideoResolution.SelectedIndex
            Case 0
                videoResolution = VIDEO_QCIF
            Case 1
                videoResolution = VIDEO_CIF
            Case 2
                videoResolution = VIDEO_VGA
            Case 3
                videoResolution = VIDEO_SVGA
            Case 4
                videoResolution = VIDEO_XVGA
            Case 5
                videoResolution = VIDEO_720P
        End Select

        AxPortSIPCoreLib1.setVideoResolution(videoResolution)

    End Sub

    Private Sub SetVideoQuality()
        If SIPInited = False Then
            Exit Sub
        End If

        Dim videoQuality As Integer = VIDEO_QUALITY_LEVEL5

        Select Case TrackBarVideoQuality.Value
            Case 1
                videoQuality = VIDEO_QUALITY_LEVEL1
            Case 2
                videoQuality = VIDEO_QUALITY_LEVEL2
            Case 3
                videoQuality = VIDEO_QUALITY_LEVEL3
            Case 4
                videoQuality = VIDEO_QUALITY_LEVEL4
            Case 5
                videoQuality = VIDEO_QUALITY_LEVEL5
            Case 6
                videoQuality = VIDEO_QUALITY_LEVEL6
        End Select

        AxPortSIPCoreLib1.setVideoQuality(videoQuality)
    End Sub

    Private Sub SIPUnRegister()
        If SIPInited = False Then
            Exit Sub
        End If

        Dim i As Integer = 0
        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetRecvCallState() = True Then
                AxPortSIPCoreLib1.rejectCall(CallSessions(i).GetSessionId(), 486, "Busy here")
            ElseIf CallSessions(i).GetSessionState() = True Then
                AxPortSIPCoreLib1.terminateCall(CallSessions(i).GetSessionId())
            End If

            CallSessions(i).Reset()
        Next



        If SIPLogined = True Then
            AxPortSIPCoreLib1.unRegisterServer()
            SIPLogined = False
        End If

        If SIPInited = True Then
            AxPortSIPCoreLib1.unInitialize()
            SIPInited = False
        End If


        'ListBoxSIPLog.Items.Clear()

    End Sub

    Private Sub UpdateAudioCodecs()
        If SIPInited = False Then
            Exit Sub
        End If

        AxPortSIPCoreLib1.clearAudioCodec()

        If CheckBoxPCMU.Checked = True Then
            AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_PCMU)
        End If

        If CheckBoxPCMA.Checked = True Then
            AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_PCMA)
        End If

        If CheckBoxG729.Checked = True Then
            AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_G729)
        End If

        If CheckBoxILBC.Checked = True Then
            AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_ILBC)
        End If

        If CheckBoxGSM.Checked = True Then
            AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_GSM)
        End If

        If CheckBoxG723.Checked = True Then
            AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_G723)
        End If

        If CheckBoxG722.Checked = True Then
            AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_G722)
        End If
        If CheckBoxSpeex.Checked = True Then
            AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_SPEEX)
        End If
        If CheckBoxAMRwb.Checked = True Then
            AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_AMRWB)
        End If
        If CheckBoxSpeexWB.Checked = True Then
            AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_SPEEXWB)
        End If
        If CheckBoxG7221.Checked = True Then
            AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_G7221)
        End If

        AxPortSIPCoreLib1.addAudioCodec(AUDIOCODEC_DTMF)

    End Sub


    Private Sub UpdateVideoCodecs()

        If SIPInited = False Then
            Exit Sub
        End If

        AxPortSIPCoreLib1.clearVideoCodec()

        If CheckBoxH263.Checked = True Then
            AxPortSIPCoreLib1.addVideoCodec(VIDEOCODEC_H263)
        End If

        If CheckBoxH2631998.Checked = True Then
            AxPortSIPCoreLib1.addVideoCodec(VIDEOCODEC_H263_1998)
        End If

        If CheckBoxH264.Checked = True Then
            AxPortSIPCoreLib1.addVideoCodec(VIDEOCODEC_H264)
        End If

    End Sub
    Private Sub ConnectUAD()
        Dim uad As UADConnector.CustomUAD = New UADConnector.CustomUAD
        AddHandler uad.OnCTIStateChange, AddressOf OnCTIStateChange
        uad.StartCommunication()
    End Sub

    Private Sub OnCTIStateChange(sender As Object, e As CTIStateChangeEventArgs)
        Try
            Statereceived(e.State)
        Catch ex As Exception

        End Try
    End Sub
    Public Delegate Sub SetStatereceivedDelegate(ByVal msg As UADConnector.CTIState)
    Public first_active As Boolean = False
    Private Sub Statereceived(ByVal msg As UADConnector.CTIState)
        If Me.InvokeRequired Then
            Dim tmp As New SetStatereceivedDelegate(AddressOf Statereceived)
            Me.Invoke(tmp, msg)
        Else
            log_maintain("UAD: " + msg.State + " - " + msg.stateobjectname)
            If msg.stateobjectname = "UADConnector.UADService.KEEPALIVETIMER" Then
                log_maintain("UAD Keep Alive: " & DateTime.Now.ToString("HH:mm:ss"))
            ElseIf msg.stateobjectname = "UADConnector.UADService.UDScreenPop" Then
                Dim ws As MOHRE_WS.Service = New MOHRE_WS.Service

                Dim udscrnpop As UADConnector.UADService.UDScreenPop
                udscrnpop = CType(msg.stateobject, UADConnector.UADService.UDScreenPop)

                Dim resp As MOHRE_WS.mapVoiceExtn_Resp = ws.mapVoiceExtn(CInt(udscrnpop.callInfoField.screenDataField.aniField), CInt(System.Configuration.ConfigurationSettings.AppSettings("uadaudioextn").ToString))
                If resp.StatusCode = 0 Then
                    LogMaintain("Mapping Saved")
                Else
                    LogMaintain("Mapping Not Saved")
                End If
            ElseIf msg.stateobjectname = "UADConnector.UADService.UDLogout" Then
                first_active = False

                Dim ws As MOHRE_WS.Service = New MOHRE_WS.Service

                Dim udscrnpop As UADConnector.EventArrival.ScreenPopEventArgs
                udscrnpop = CType(msg.stateobject, UADConnector.EventArrival.ScreenPopEventArgs)

                Dim resp As Boolean = ws.ResetAgentAudioExt(Int(System.Configuration.ConfigurationSettings.AppSettings("uadaudioextn").ToString))
                If resp = True Then
                    LogMaintain("Mapping Cleared")
                Else
                    LogMaintain("Mapping Not Cleared")
                End If
                Me.Close()
            ElseIf msg.stateobjectname = "UADConnector.UADService.UDCCDown" Or msg.stateobjectname = "UADConnector.UADService.UDLogout" Or
         msg.stateobjectname = "UADConnector.UADService.UDWrapTimeout" Or msg.stateobjectname = "UADConnector.UADService.UDWrap" Or
         msg.stateobjectname = "UADConnector.UADService.UDNotReady" Or msg.stateobjectname = "UADConnector.UADService.UDIdle" Then

                first_active = False

                Dim ws As MOHRE_WS.Service = New MOHRE_WS.Service

                Dim resp As Boolean = ws.ResetAgentAudioExt(Int(System.Configuration.ConfigurationSettings.AppSettings("uadaudioextn").ToString))
                If resp = True Then
                    LogMaintain("Mapping Cleared")
                Else
                    LogMaintain("Mapping Not Cleared")
                End If
                CallHangup()
            ElseIf msg.stateobjectname = "UADConnector.UADService.UDActive" Then
                If first_active = False Then
                    first_active = True
                Else
                    videounhold()
                End If
            ElseIf msg.stateobjectname = "UADConnector.UADService.UDHeld" Then
                videohold()
            End If
        End If
    End Sub
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "CSI SIP Phone V " & Application.ProductVersion & " - " & username
        Me.Hide()
        'Me.WindowState = FormWindowState.Minimized
        Me.NotifyIcon1.Icon = Me.Icon
        'Me.NotifyIcon1.Visible = False
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            GroupBox16.Enabled = True
        End If
        Me.NotifyIcon1.Text = "SipPhone Developed By CSI"
        If System.Configuration.ConfigurationSettings.AppSettings("media") = "audio" Then
            Me.Width = 593
            Me.Height = 346
            PictureBox3.Left = 526
            PictureBox3.Top = 24
            TextBoxPhoneNumber.Text = System.Configuration.ConfigurationSettings.AppSettings("dialingqueue")
        Else
            Me.Width = 869
            Me.Height = 346
            PictureBox3.Left = 802
            PictureBox3.Top = 24
            TextBoxPhoneNumber.Text = System.Configuration.ConfigurationSettings.AppSettings("videoqueue")
        End If


        frmKeypad.Show()
        frmKeypad.Hide()
        'MsgBox("This sample was built base on evaluation PortSIP VoIP SDK for desktop, which allows only two minutes conversation. The conversation will be cut off automatically after two minutes, then you can't hearing anything. The official SDK does not has this limitation, please feel free to contact us at: sales@portsip.com if you have any quesitons.")

        ' Create the call sessions array, the PortSIP VOIP SDK .0 has allows maximum 100 lines,
        ' but we just use 8 lines with this sample, we need a class to save the call sessions information
        TextBoxUserName.Text = System.Configuration.ConfigurationSettings.AppSettings("extn")
        TextBoxPassword.Text = System.Configuration.ConfigurationSettings.AppSettings("extn")
        TextBoxServer.Text = System.Configuration.ConfigurationSettings.AppSettings("serverip")
        TextBoxServerPort.Text = System.Configuration.ConfigurationSettings.AppSettings("serverport")
        Dim i As Integer = 0
        For i = 0 To MAX_LINES
            CallSessions(i) = New Sessions
            CallSessions(i).Reset()
        Next


        AxDeviceManagerLib1.initialize()
        Dim Volume As Integer = 0
        Volume = AxDeviceManagerLib1.getAudioOutputVolume(0) ' Get the audio playback device volume, 0 means the defalut device of System

        If Volume >= 0 And Volume <= 65535 Then
            TrackBarSpeaker.Value = Volume
        End If

        Volume = AxDeviceManagerLib1.getAudioInputVolume(0)

        If Volume >= 0 And Volume <= 65535 Then
            TrackBarMicrophone.Value = Volume
        End If




        Dim DeviceCount As Integer = 0
        Dim DeviceName As String = ""

        DeviceCount = AxDeviceManagerLib1.getAudioOutputDeviceNums()
        For i = 0 To DeviceCount - 1
            AxDeviceManagerLib1.getAudioOutputDeviceName(i, DeviceName)
            ComboBoxSpeakers.Items.Add(DeviceName)
        Next

        If ComboBoxSpeakers.Items.Count > 0 Then
            ComboBoxSpeakers.SelectedIndex = 0
        End If
        'cntDeviceCount = DeviceCount


        DeviceCount = AxDeviceManagerLib1.getAudioInputDeviceNums()
        For i = 0 To DeviceCount - 1
            AxDeviceManagerLib1.getAudioInputDeviceName(i, DeviceName)
            ComboBoxMicrophones.Items.Add(DeviceName)
        Next

        If ComboBoxMicrophones.Items.Count > 0 Then
            ComboBoxMicrophones.SelectedIndex = 0
        End If



        DeviceCount = AxDeviceManagerLib1.getVideoDeviceNums()
        For i = 0 To DeviceCount - 1
            AxDeviceManagerLib1.getVideoDeviceName(i, DeviceName)
            ComboBoxCameras.Items.Add(DeviceName)
        Next

        If ComboBoxCameras.Items.Count > 0 Then
            ComboBoxCameras.SelectedIndex = 0
        End If

        ComboBoxTransport.Items.Add("UDP")
        ComboBoxTransport.Items.Add("TCP")
        ComboBoxTransport.Items.Add("TLS")

        ComboBoxTransport.SelectedIndex = 0

        ComboBoxSRTP.Items.Add("None")
        ComboBoxSRTP.Items.Add("Prefer")
        ComboBoxSRTP.Items.Add("Force")
        ComboBoxSRTP.SelectedIndex = 0

        ComboBoxVideoResolution.Items.Add("QCIF")
        ComboBoxVideoResolution.Items.Add("CIF")
        ComboBoxVideoResolution.Items.Add("VGA")
        ComboBoxVideoResolution.Items.Add("SVGA")
        ComboBoxVideoResolution.Items.Add("XVGA")
        ComboBoxVideoResolution.Items.Add("720P")
        ComboBoxVideoResolution.SelectedIndex = 0

        ComboBoxLines.Items.Add("Line-1")
        ComboBoxLines.Items.Add("Line-2")
        ComboBoxLines.Items.Add("Line-3")
        ComboBoxLines.Items.Add("Line-4")
        ComboBoxLines.Items.Add("Line-5")
        ComboBoxLines.Items.Add("Line-6")
        ComboBoxLines.Items.Add("Line-7")
        ComboBoxLines.Items.Add("Line-8")


        ComboBoxLines.SelectedIndex = 0

        SIPInited = False
        SIPLogined = False
        CurrentlyLine = LINE_BASE
        log_maintain("Initializing ...")
        'Button1_Click(sender, e)
        Timer1.Enabled = True
        SocketThread = New System.Threading.Thread(New System.Threading.ThreadStart(AddressOf ListenerInititialize))
        SocketThread.IsBackground = True
        SocketThread.Start()
        ConnectUAD()
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        ''If Me.WindowState = FormWindowState.Normal Then
        ''    e.Cancel = True
        ''    Me.Hide()
        ''    Me.WindowState = FormWindowState.Minimized
        ''    Me.NotifyIcon1.Visible = True
        ''Else
        Try
                Button2_Click(sender, e)
                AxDeviceManagerLib1.unInitialize()
                SIPUnRegister()
                log_maintain("UnInitialized")
                If e.CloseReason = CloseReason.ApplicationExitCall Then
                    log_maintain("loseReason=ApplicationExitCall")
                ElseIf e.CloseReason = CloseReason.FormOwnerClosing Then
                    log_maintain("CloseReason=FormOwnerClosing")
                ElseIf e.CloseReason = CloseReason.MdiFormClosing Then
                    log_maintain("CloseReason=MdiFormClosing")
                ElseIf e.CloseReason = CloseReason.None Then
                    log_maintain("CloseReason=None")
                ElseIf e.CloseReason = CloseReason.TaskManagerClosing Then
                    log_maintain("CloseReason=TaskManagerClosing")
                ElseIf e.CloseReason = CloseReason.UserClosing Then
                    log_maintain("CloseReason=UserClosing")
                ElseIf e.CloseReason = CloseReason.WindowsShutDown Then
                    log_maintain("CloseReason=WindowsShutDown")
                End If
                serverSocket.Stop()
                If SocketThread.IsAlive = True Then
                    SocketThread.Abort()
                End If
                Application.ExitThread()
                log_maintain("End")
            Catch ex As Exception
                log_maintain(ex.StackTrace)
                log_maintain("End")
            End Try
        'End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If SIPInited = True Then
            Exit Sub
        End If

        If SIPLogined = True Then
            Exit Sub
        End If
        If cntDeviceCount = 0 Then
            log_maintain("Registration Failed due to No Audio Devices found")
            Exit Sub
        End If
        If TextBoxUserName.Text.Length <= 0 Then
            MsgBox("The user name does not allows empty.")
            Exit Sub
        End If

        If TextBoxPassword.Text.Length <= 0 Then
            MsgBox("The password does not allows empty.")
            Exit Sub
        End If

        If TextBoxServer.Text.Length <= 0 Then
            MsgBox("The proxy server does not allows empty.")
            Exit Sub
        End If

        If TextBoxServerPort.Text.Length <= 0 Then
            MsgBox("The proxy server port does not allows empty.")
            Exit Sub
        End If

        Dim ProxyPort As Integer = CInt(TextBoxServerPort.Text)
        If ProxyPort > 65535 Or ProxyPort < 0 Then
            MsgBox("The proxy server port out of range.")
            Exit Sub
        End If

        Dim StunServerPort As Integer
        If TextBoxStunPort.Text.Length > 0 Then
            StunServerPort = CInt(TextBoxStunPort.Text)
            If StunServerPort > 65535 Or StunServerPort < 0 Then
                MsgBox("The stun server port out of range.")
                Exit Sub
            End If
        End If



        Dim instance As New Random
        Dim LocalSIPPort As Integer = instance.Next(1000, 5000) + 4000 ' Generate a random port for SIP

        Dim LocalIP As String = ""
        AxDeviceManagerLib1.getLocalIP(0, LocalIP)

        Dim TransportType As Integer = TRANSPORT_UDP

        Select Case ComboBoxTransport.SelectedIndex
            Case 0
                TransportType = TRANSPORT_UDP
            Case 1
                TransportType = TRANSPORT_TCP
            Case 2
                TransportType = TRANSPORT_TLS
        End Select

        Dim outboundServer As String = ""
        Dim outboundServerPort As Integer = 0
        vextn = TextBoxUserName.Text
        Dim ret = AxPortSIPCoreLib1.initialize(TransportType,
                                                 PORTSIP_LOG_INFO,
                                                 MAX_LINES,
                                                 TextBoxUserName.Text,
                                                 TextBoxDisplayName.Text,
                                                 TextBoxAuthName.Text,
                                                 TextBoxPassword.Text,
                                                 "PortSIP VoIP SDK 5.0",
                                                 LocalIP,
                                                 LocalSIPPort,
                                                 TextBoxUserDomain.Text,
                                                 TextBoxServer.Text,
                                                 ProxyPort,
                                                 outboundServer,
                                                 outboundServerPort,
                                                 TextBoxStunServer.Text,
                                                 StunServerPort)
        If ret = -1 Then
            Me.Show()
            Me.WindowState = FormWindowState.Normal
            MsgBox("initialize failed")
            Exit Sub
        End If

        SIPInited = True

        SetSRTPType()

        AxPortSIPCoreLib1.setLicenseKey("zmeR9P0ySmbnVdM35kw7QQ==@KYIiPHJmpiXMxkBvLGG8dw==@JfyuRmfb9uiTGgVpAiAOAQ==@Yd8oVZG9ULLdf/kWby4Uyw==")
        If System.Configuration.ConfigurationSettings.AppSettings("enableStackLog") = "1" Then
            AxPortSIPCoreLib1.enableStackLog()
        End If
        AxPortSIPCoreLib1.enableAEC(1) ' Enable AEC
        If System.Configuration.ConfigurationSettings.AppSettings("enableSessionTimer") = "1" Then
            AxPortSIPCoreLib1.enableSessionTimer(System.Configuration.ConfigurationSettings.AppSettings("sessionT"))
        End If
        If System.Configuration.ConfigurationSettings.AppSettings("RtpKeepAlive") = "1" Then
            AxPortSIPCoreLib1.setRtpKeepAlive(1)
        End If
        ' 30 is the local video image left of sample application window, 600 is the local video image top, 176 is the video width, 144 is the video height
        ' 216 is the remote video image left of sample application window, 600 is the remote video image top, 176 is the video width, 144 is the video height

        'AxPortSIPCoreLib1.setVideoImagePos(Me.Handle, 30, 600, 176, 144, Me.Handle, 216, 600, 176, 144)

        AxPortSIPCoreLib1.setVideoImagePos(BigVideo.Handle, 0, 0, BigVideo.Width, BigVideo.Height, SmallVideo.Handle, 0, 0, SmallVideo.Width, SmallVideo.Height)

        SetVideoResolution()
        SetVideoQuality()

        UpdateAudioCodecs()
        UpdateVideoCodecs()


        InitSettings()

        If AxPortSIPCoreLib1.registerServer(300) = -1 Then
            SIPInited = False
            AxPortSIPCoreLib1.unInitialize()
            MsgBox("registerServer failed")

            Me.Show()
            Me.WindowState = FormWindowState.Normal
            MsgBox("initialize failed")
        End If

        AxPortSIPCoreLib1.reverseSendingVideo(0)
        AxPortSIPCoreLib1.reverseReceivedVideo(0)
        log_maintain("Registering...")

    End Sub
    Dim dispwindow As New DisplayWindow
    Public displaywindowleft As Integer
    Public displaywindowtop As Integer
    Public displaywindowwidth As Integer
    Public displaywindowheight As Integer
    Public Function displaywindowposition(ByVal windowleft As Integer, ByVal windowtop As Integer, ByVal windowwidth As Integer, ByVal windowheight As Integer) As Boolean
        displaywindowleft = windowleft
        displaywindowtop = windowtop
        displaywindowwidth = windowwidth
        displaywindowheight = windowheight
        displaywindowposition = True

    End Function
    Public Sub videohold()
        If System.Configuration.ConfigurationSettings.AppSettings("media") = "video" Then
            If SIPInited = False Or SIPLogined = False Then
                Exit Sub
            End If
            If CallSessions(CurrentlyLine).GetSessionState() = False Then
                Exit Sub
            End If
            Dim ret As Integer = -1
            ret = AxPortSIPCoreLib1.sendVideoStream(CallSessions(CurrentlyLine).GetSessionId(), 0)
            If ret = 0 Then
                log_maintain("video streaming successfully hold")
            Else
                log_maintain("video streaming hold failed")
            End If
            If SIPInited = False Then
                Exit Sub
            End If
            AxPortSIPCoreLib1.viewLocalVideo(0)
        End If
    End Sub
    Public Sub videounhold()
        If System.Configuration.ConfigurationSettings.AppSettings("media") = "video" Then
            If SIPInited = False Or SIPLogined = False Then
                Exit Sub
            End If
            If CallSessions(CurrentlyLine).GetSessionState() = False Then
                Exit Sub
            End If
            Dim ret As Integer = -1
            ret = AxPortSIPCoreLib1.sendVideoStream(CallSessions(CurrentlyLine).GetSessionId(), 1)
            If ret = 0 Then
                log_maintain("video streaming successfully unhold")
            Else
                log_maintain("video streaming unhold failed")
            End If
            If SIPInited = False Then
                Exit Sub
            End If
            AxPortSIPCoreLib1.viewLocalVideo(1)
        End If
    End Sub
    Public Sub videoenabled()
        If System.Configuration.ConfigurationSettings.AppSettings("media") = "video" Then
            'videoenabledstatus = True
            'Visible = True
            videojoin()
            If SIPInited = False Or SIPLogined = False Then
                Exit Sub
            End If

            If CallSessions(CurrentlyLine).GetSessionState() = False Then
                Exit Sub
            End If

            Dim ret As Integer = -1
            ret = AxPortSIPCoreLib1.sendVideoStream(CallSessions(CurrentlyLine).GetSessionId(), 1)
            If ret = 0 Then
                log_maintain("video streaming successfully started")
            Else
                log_maintain("video streaming start failed")
            End If
            If SIPInited = False Then
                Exit Sub
            End If
            AxPortSIPCoreLib1.viewLocalVideo(1)
            'TextBoxRecordFilePath.Text = System.Configuration.ConfigurationSettings.AppSettings("Recordingpath")
            ' TextBoxRecordFilePath.Text = "C:\Documents and Settings\Development\Desktop\recording"
            'TextBoxVideoRecordFileName.Text = Date.Now.ToString("ddMMyyyy HHmmss")
            'recstart()
        End If
    End Sub
    Public Sub videodisabled()
        If System.Configuration.ConfigurationSettings.AppSettings("media") = "video" Then
            'videoenabledstatus = False
            'Visible = False
            videouninitiate()
            If SIPInited = False Or SIPLogined = False Then
                Exit Sub
            End If
            If CallSessions(CurrentlyLine).GetSessionState() = False Then
                Exit Sub
            End If
            Dim ret As Integer = -1
            ret = AxPortSIPCoreLib1.sendVideoStream(CallSessions(CurrentlyLine).GetSessionId(), 0)
            If ret = 0 Then
                log_maintain("video streaming successfully stoped")
            Else
                log_maintain("video streaming stop failed")
            End If
            If SIPInited = False Then
                Exit Sub
            End If
            AxPortSIPCoreLib1.viewLocalVideo(0)

            'recstop()
        End If
    End Sub
    Public videoenabledstatus As Boolean = False
    Public Sub videoinitiate()
        If System.Configuration.ConfigurationSettings.AppSettings("media") = "video" Then
            SmallVideo.Visible = False
            videoenabledstatus = True
            'displaywindowposition(0, 0, 0, 0)
            'dispwindow.Show()
            AxPortSIPCoreLib1.setVideoImagePos(BigVideo.Handle, 0, 0, BigVideo.Width, BigVideo.Height, SmallVideo.Handle, 0, 0, SmallVideo.Width, SmallVideo.Height)
        SetVideoResolution()
        SetVideoQuality()
        AxPortSIPCoreLib1.viewLocalVideo(1)

            'UpdateAudioCodecs()
            'UpdateVideoCodecs()
        End If
    End Sub
    Public Sub videojoin()
        If System.Configuration.ConfigurationSettings.AppSettings("media") = "video" Then
            SmallVideo.Visible = True
            videoenabledstatus = True
            AxPortSIPCoreLib1.setVideoImagePos(SmallVideo.Handle, 0, 0, SmallVideo.Width, SmallVideo.Height, BigVideo.Handle, 0, 0, BigVideo.Width, BigVideo.Height)
            SetVideoResolution()
            SetVideoQuality()
        End If
    End Sub
    Public Sub videouninitiate()
        If System.Configuration.ConfigurationSettings.AppSettings("media") = "video" Then
            SmallVideo.Visible = False
            videoenabledstatus = False
            AxPortSIPCoreLib1.setVideoImagePos(BigVideo.Handle, 0, 0, BigVideo.Width, BigVideo.Height, SmallVideo.Handle, 0, 0, SmallVideo.Width, SmallVideo.Height)
            SetVideoResolution()
            SetVideoQuality()
            AxPortSIPCoreLib1.viewLocalVideo(1)
        End If
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Sip_Unregister()
    End Sub
    Private Sub Sip_Unregister()
        If SIPInited = False Then
            Exit Sub
        End If
        ListBoxSIPLog.Items.Clear()
        SipUnregister()
    End Sub
    Private Sub ComboBoxLines_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxLines.SelectedIndexChanged
        If SIPInited = False Or SIPLogined = False Then
            ComboBoxLines.SelectedIndex = 0
            Exit Sub
        End If

        Dim Line As Integer = ComboBoxLines.SelectedIndex + LINE_BASE

        If CurrentlyLine = Line Then
            Exit Sub
        End If

        If CheckBoxConf.Checked = True Then
            CurrentlyLine = ComboBoxLines.SelectedIndex + LINE_BASE
            Exit Sub
        End If

        ' To switch the line, must hold currently line first
        If CallSessions(CurrentlyLine).GetSessionState() = True And CallSessions(CurrentlyLine).GetHoldState() = False Then
            AxPortSIPCoreLib1.hold(CallSessions(CurrentlyLine).GetSessionId())
            CallSessions(CurrentlyLine).SetHoldState(True)

            Dim Text As String = "Line " + CStr(CurrentlyLine)
            Text = Text + ": Hold"
            log_maintain(Text)
        End If

        CurrentlyLine = ComboBoxLines.SelectedIndex + LINE_BASE

        ' If target line was in hold state, then un-hold it
        If CallSessions(CurrentlyLine).GetSessionState() = True And CallSessions(CurrentlyLine).GetHoldState() = True Then
            AxPortSIPCoreLib1.unHold(CallSessions(CurrentlyLine).GetSessionId())
            CallSessions(CurrentlyLine).SetHoldState(False)

            Dim Text As String = "Line " + CStr(CurrentlyLine)
            Text = Text + ": UnHold - call established"
            videoenabled()
            log_maintain(Text)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            TextBoxPhoneNumber.Text = TextBoxPhoneNumber.Text + "1"
        End If
        sendDTMF(1)
    End Sub
    Public Sub sendDTMF(ByVal dtmftone As Integer)
        If System.Configuration.ConfigurationSettings.AppSettings("dtmf") = "true" Then
            If SIPInited = True And CallSessions(CurrentlyLine).GetSessionState() = True Then
                AxPortSIPCoreLib1.sendDtmf(CallSessions(CurrentlyLine).GetSessionId(), dtmftone)
            End If
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            TextBoxPhoneNumber.Text = TextBoxPhoneNumber.Text + "2"
        End If
        sendDTMF(2)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            TextBoxPhoneNumber.Text = TextBoxPhoneNumber.Text + "3"
        End If
        sendDTMF(3)
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            TextBoxPhoneNumber.Text = TextBoxPhoneNumber.Text + "4"
        End If
        sendDTMF(4)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            TextBoxPhoneNumber.Text = TextBoxPhoneNumber.Text + "5"
        End If
        sendDTMF(5)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            TextBoxPhoneNumber.Text = TextBoxPhoneNumber.Text + "6"
        End If
        sendDTMF(6)
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            TextBoxPhoneNumber.Text = TextBoxPhoneNumber.Text + "7"
        End If
        sendDTMF(7)
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            TextBoxPhoneNumber.Text = TextBoxPhoneNumber.Text + "8"
        End If
        sendDTMF(8)
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            TextBoxPhoneNumber.Text = TextBoxPhoneNumber.Text + "9"
        End If
        sendDTMF(9)
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            TextBoxPhoneNumber.Text = TextBoxPhoneNumber.Text + "*"
        End If
        sendDTMF(10)
    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            TextBoxPhoneNumber.Text = TextBoxPhoneNumber.Text + "0"
        End If
        sendDTMF(0)
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        If System.Configuration.ConfigurationSettings.AppSettings("outbound") = "true" Then
            TextBoxPhoneNumber.Text = TextBoxPhoneNumber.Text + "#"
        End If
        sendDTMF(11)
    End Sub

    Private Sub ButtonDial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDial.Click
        DialToQueue()
        'If SIPInited = False Or SIPLogined = False Then
        '    Exit Sub
        'End If

        'If TextBoxPhoneNumber.Text.Length <= 0 Then
        '    MsgBox("The phone number is empty.")
        '    Exit Sub
        'End If

        'If CallSessions(CurrentlyLine).GetSessionState() = True Or CallSessions(CurrentlyLine).GetRecvCallState() = True Then
        '    MsgBox("Current line is busy now, please switch a line.")
        '    Exit Sub
        'End If


        'Dim callto As String = TextBoxPhoneNumber.Text


        'UpdateAudioCodecs()
        'UpdateVideoCodecs()

        'If AxPortSIPCoreLib1.isAudioCodecEmpty() = 1 Then
        '    InitDefaultAudioCodecs()
        'End If


        '' Usually for 3PCC need to make call without SDP
        'Dim hasSDP As Long = 1
        'If CheckBoxSDP.Checked = True Then
        '    hasSDP = 0
        'End If

        'AxPortSIPCoreLib1.setAudioDeviceId(ComboBoxMicrophones.SelectedIndex, ComboBoxSpeakers.SelectedIndex)
        'AxPortSIPCoreLib1.setVideoDeviceId(ComboBoxCameras.SelectedIndex)
        'Dim sessionId As Long = AxPortSIPCoreLib1.call(callto, hasSDP)
        'If sessionId <= 0 Then
        '    log_maintain("Call failed")
        '    Exit Sub
        'End If

        'CallSessions(CurrentlyLine).SetSessionId(sessionId)
        'CallSessions(CurrentlyLine).SetSessionState(True)

        'Dim Text As String = "Line " + CStr(CurrentlyLine)
        'Text = Text + ": Calling..."
        ''AxPortSIPCoreLib1.playLocalWaveFile(Application.StartupPath & "\test.wav")
        'AxWindowsMediaPlayer1.settings.autoStart = True
        'AxWindowsMediaPlayer1.settings.playCount = 100
        'AxWindowsMediaPlayer1.URL = Application.StartupPath & "\test.wav" ' System.Configuration.ConfigurationSettings.AppSettings("ringpath")
        'log_maintain(Text)
        'videoinitiate()
    End Sub
    Public dialingqueue As String = System.Configuration.ConfigurationSettings.AppSettings("dialingqueue").ToString
    Public videoqueue As String = System.Configuration.ConfigurationSettings.AppSettings("videoqueue").ToString
    Public Sub DialToQueue()
        If SIPInited = False Or SIPLogined = False Then
            Exit Sub
        End If

        'If TextBoxPhoneNumber.Text.Length <= 0 Then
        '    log_maintain("The phone number is empty.")
        '    Exit Sub
        'End If

        If CallSessions(CurrentlyLine).GetSessionState() = True Or CallSessions(CurrentlyLine).GetRecvCallState() = True Then
            log_maintain("Current line is busy now, please switch a line.")
            Exit Sub
        End If


        Dim callto As String
        If System.Configuration.ConfigurationSettings.AppSettings("media") = "audio" Then
            callto = dialingqueue
        Else
            callto = videoqueue
        End If

        UpdateAudioCodecs()
        UpdateVideoCodecs()

        If AxPortSIPCoreLib1.isAudioCodecEmpty() = 1 Then
            InitDefaultAudioCodecs()
        End If


        ' Usually for 3PCC need to make call without SDP
        Dim hasSDP As Long = 1
        If CheckBoxSDP.Checked = True Then
            hasSDP = 0
        End If

        AxPortSIPCoreLib1.setAudioDeviceId(ComboBoxMicrophones.SelectedIndex, ComboBoxSpeakers.SelectedIndex)
        If System.Configuration.ConfigurationSettings.AppSettings("media") = "video" Then
            AxPortSIPCoreLib1.setVideoDeviceId(ComboBoxCameras.SelectedIndex)
        End If
        Dim sessionId As Long = AxPortSIPCoreLib1.call(callto, hasSDP)
        If sessionId <= 0 Then
            log_maintain("Call failed")
            Exit Sub
        End If

        CallSessions(CurrentlyLine).SetSessionId(sessionId)
        CallSessions(CurrentlyLine).SetSessionState(True)

        Dim Text As String = "Line " + CStr(CurrentlyLine)
        Text = Text + ": Calling..."
        'AxPortSIPCoreLib1.playLocalWaveFile(Application.StartupPath & "\test.wav")
        AxWindowsMediaPlayer1.settings.autoStart = True
        AxWindowsMediaPlayer1.settings.playCount = 100
        AxWindowsMediaPlayer1.URL = Application.StartupPath & "\test.wav" ' System.Configuration.ConfigurationSettings.AppSettings("ringpath")
        log_maintain(Text)
        videoinitiate()
    End Sub
    Private Sub ButtonHangUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonHangUp.Click
        CallHangup()
    End Sub
    Private Sub CallHangup()
        If SIPInited = False Or SIPLogined = False Then
            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetRecvCallState() = True Then
            AxPortSIPCoreLib1.rejectCall(CallSessions(CurrentlyLine).GetSessionId(), 486, "Busy here")
            CallSessions(CurrentlyLine).Reset()

            Dim Text As String = "Line " + CStr(CurrentlyLine)
            Text = Text + ": Rejected call"
            log_maintain(Text)

            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetSessionState() = True Then
            AxPortSIPCoreLib1.terminateCall(CallSessions(CurrentlyLine).GetSessionId())
            CallSessions(CurrentlyLine).Reset()

            Dim Text As String = "Line " + CStr(CurrentlyLine)
            Text = Text + ": Hang up"
            AxWindowsMediaPlayer1.URL = ""
            'AxPortSIPCoreLib1.playLocalWaveFile("")
            log_maintain(Text)
            videodisabled()
        End If
    End Sub
    Public Sub QueueEnd()
        If SIPInited = False Or SIPLogined = False Then
            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetRecvCallState() = True Then
            AxPortSIPCoreLib1.rejectCall(CallSessions(CurrentlyLine).GetSessionId(), 486, "Busy here")
            CallSessions(CurrentlyLine).Reset()

            Dim Text As String = "Line " + CStr(CurrentlyLine)
            Text = Text + ": Rejected call"
            log_maintain(Text)

            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetSessionState() = True Then
            AxPortSIPCoreLib1.terminateCall(CallSessions(CurrentlyLine).GetSessionId())
            CallSessions(CurrentlyLine).Reset()

            Dim Text As String = "Line " + CStr(CurrentlyLine)
            Text = Text + ": Hang up"
            AxWindowsMediaPlayer1.URL = ""
            'AxPortSIPCoreLib1.playLocalWaveFile("")
            log_maintain(Text)
            videodisabled()
        End If
    End Sub
    Private Sub ButtonReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReject.Click
        If SIPInited = False Or SIPLogined = False Then
            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetRecvCallState() = True Then
            AxPortSIPCoreLib1.rejectCall(CallSessions(CurrentlyLine).GetSessionId(), 486, "Busy here")
            CallSessions(CurrentlyLine).Reset()

            Dim Text As String = "Line " + CStr(CurrentlyLine)
            Text = Text + ": Rejected call"
            log_maintain(Text)
        End If
    End Sub

    Private Sub ButtonHold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonHold.Click
        If SIPInited = False Or SIPLogined = False Then
            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetSessionState() = False Then
            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetHoldState() = True Then
            Exit Sub
        End If


        AxPortSIPCoreLib1.hold(CallSessions(CurrentlyLine).GetSessionId())
        CallSessions(CurrentlyLine).SetHoldState(True)

        Dim Text As String = "Line " + CStr(CurrentlyLine)
        Text = Text + ": hold"
        log_maintain(Text)
    End Sub

    Private Sub Button16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button16.Click
        If SIPInited = False Or SIPLogined = False Then
            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetSessionState() = False Then
            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetHoldState() = False Then
            Exit Sub
        End If


        AxPortSIPCoreLib1.unHold(CallSessions(CurrentlyLine).GetSessionId())
        CallSessions(CurrentlyLine).SetHoldState(False)

        Dim Text As String = "Line " + CStr(CurrentlyLine)
        Text = Text + ": Un-Hold"
        log_maintain(Text)
    End Sub

    Private Sub ButtonTransfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTransfer.Click
        If SIPInited = False Or SIPLogined = False Then
            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetSessionState() = False Then
            MsgBox("Need to make the call established first")
            Exit Sub
        End If

        If Form2.ShowDialog() <> DialogResult.OK Then
            Exit Sub
        End If

        If Form2.GetTransferNumber().Length <= 0 Then
            MsgBox("The transfer number is empty")
            Exit Sub
        End If

        Dim sessionId As Long = CallSessions(CurrentlyLine).GetSessionId()
        Dim ReferTo As String = Form2.GetTransferNumber()


        Dim Text As String
        Text = ""


        If AxPortSIPCoreLib1.refer(sessionId, ReferTo) = -1 Then
            Text = "Line " + CStr(CurrentlyLine)
            Text = Text + ": Transfer failed"
            log_maintain(Text)


            Exit Sub
        End If


        Text = "Line " + CStr(CurrentlyLine)
        Text = Text + ": Transfering"
        log_maintain(Text)
    End Sub

    Private Sub ButtonAnswer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAnswer.Click
        If SIPInited = False Or SIPLogined = False Then
            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetRecvCallState() = False Then
            MsgBox("Current line does not has incoming call, please switch a line.")
            Exit Sub
        End If

        CallSessions(CurrentlyLine).SetRecvCallState(False)
        CallSessions(CurrentlyLine).SetSessionState(True)

        AxPortSIPCoreLib1.answerCall(CallSessions(CurrentlyLine).GetSessionId())

        Dim Text As String = "Line " + CStr(CurrentlyLine)
        Text = Text + ": Call established"
        AxWindowsMediaPlayer1.URL = ""
        'AxPortSIPCoreLib1.playLocalWaveFile("")
        videoenabled()
        log_maintain(Text)
    End Sub

    Private Sub Button22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button22.Click
        ListBoxSIPLog.Items.Clear()
    End Sub

    Private Sub CheckBoxConf_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxConf.CheckedChanged
        If SIPInited = False Or SIPLogined = False Then
            CheckBoxConf.Checked = False
            Exit Sub
        End If

        If CheckBoxConf.Checked = True Then
            ' Start conference
            If AxPortSIPCoreLib1.createConference() = 0 Then
                log_maintain("Create conference succeeded.")
            Else
                log_maintain("Create conference failed.")
            End If

        Else
            ' Stop conference
            ' Before stop the conference, MUST place all lines to hold state

            Dim sessionIds(9) As Long
            Dim i As Long = 0
            For i = LINE_BASE To MAX_LINES
                sessionIds(i) = 0
            Next

            For i = LINE_BASE To MAX_LINES
                If CallSessions(i).GetSessionState() = True And CallSessions(i).GetHoldState() = False Then
                    sessionIds(i) = CallSessions(i).GetSessionId()

                    ' hold it first
                    AxPortSIPCoreLib1.hold(sessionIds(i))
                    CallSessions(i).SetHoldState(True)

                    Dim Text As String = "Line " + CStr(i)
                    Text = Text + ": Hold"
                    log_maintain(Text)

                End If
            Next

            AxPortSIPCoreLib1.destroyConference()

            log_maintain("Taken off Conference")

        End If
    End Sub

    Private Sub TrackBarSpeaker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AxDeviceManagerLib1.setAudioOutputVolume(ComboBoxSpeakers.SelectedIndex, TrackBarSpeaker.Value)

    End Sub

    Private Sub TrackBarMicrophone_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AxDeviceManagerLib1.setAudioInputVolume(ComboBoxMicrophones.SelectedIndex, TrackBarMicrophone.Value)

    End Sub

    Private Sub ButtonTestAudio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTestAudio.Click
        If ButtonTestAudio.Text = "Test Audio" Then
            AxDeviceManagerLib1.startAudioPlayLoopbackTest(ComboBoxMicrophones.SelectedIndex, ComboBoxSpeakers.SelectedIndex)
            ButtonTestAudio.Text = "Stop Test"
        Else
            AxDeviceManagerLib1.stopAudioPlayLoopbackTest()
            ButtonTestAudio.Text = "Test Audio"
        End If
    End Sub

    Private Sub CheckBoxMute_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxMute.CheckedChanged
        If SIPInited = False Or SIPLogined = False Then
            Exit Sub
        End If

        If CheckBoxMute.Checked = True Then
            AxPortSIPCoreLib1.muteMicrophone(1)
        Else
            AxPortSIPCoreLib1.muteMicrophone(0)
        End If
    End Sub

    Private Sub ButtonVideoPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonVideoPreview.Click
        If ButtonVideoPreview.Text = "Video Preview" Then
            AxDeviceManagerLib1.startVideoPreview(Me.Handle, ComboBoxCameras.SelectedIndex, 30, 600, 176, 144)
            ButtonVideoPreview.Text = "Stop Preview"
        Else
            AxDeviceManagerLib1.stopVideoPreview()
            ButtonVideoPreview.Text = "Video Preview"
        End If
    End Sub

    Private Sub ButtonCameraOptions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCameraOptions.Click
        AxDeviceManagerLib1.showCameraPropertyWindow(Me.Handle, ComboBoxCameras.SelectedIndex)
    End Sub

    Private Sub ButtonLocalVideo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonLocalVideo.Click
        If SIPInited = False Then
            Exit Sub
        End If

        If ButtonLocalVideo.Text = "Local Video" Then
            AxPortSIPCoreLib1.viewLocalVideo(1)
            ButtonLocalVideo.Text = "Stop Local"
        Else
            AxPortSIPCoreLib1.viewLocalVideo(0)
            ButtonLocalVideo.Text = "Local Video"
        End If


    End Sub

    Private Sub ButtonSendVideo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSendVideo.Click
        If SIPInited = False Or SIPLogined = False Then
            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetSessionState() = False Then
            Exit Sub
        End If


        AxPortSIPCoreLib1.sendVideoStream(CallSessions(CurrentlyLine).GetSessionId(), 1)
    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
        If SIPInited = False Or SIPLogined = False Then
            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetSessionState() = False Then
            Exit Sub
        End If

        AxPortSIPCoreLib1.sendVideoStream(CallSessions(CurrentlyLine).GetSessionId(), 0)
    End Sub

    Private Sub CheckBoxPCMU_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxPCMU.CheckedChanged
        UpdateAudioCodecs()
    End Sub

    Private Sub CheckBoxPCMA_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxPCMA.CheckedChanged
        UpdateAudioCodecs()
    End Sub

    Private Sub CheckBoxG729_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxG729.CheckedChanged
        UpdateAudioCodecs()
    End Sub

    Private Sub CheckBoxILBC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxILBC.CheckedChanged
        UpdateAudioCodecs()
    End Sub

    Private Sub CheckBoxGSM_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxGSM.CheckedChanged
        UpdateAudioCodecs()
    End Sub

    Private Sub CheckBoxG723_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxG723.CheckedChanged
        UpdateAudioCodecs()
    End Sub

    Private Sub CheckBoxG722_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxG722.CheckedChanged
        UpdateAudioCodecs()
    End Sub
    Private Sub CheckBoxSpeex_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxSpeex.CheckedChanged
        UpdateAudioCodecs()
    End Sub
    Private Sub CheckBoxAMRwb_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxAMRwb.CheckedChanged
        UpdateAudioCodecs()
    End Sub
    Private Sub CheckBoxSpeexWB_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxSpeexWB.CheckedChanged
        UpdateAudioCodecs()
    End Sub
    Private Sub CheckBoxG7221_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxG7221.CheckedChanged
        UpdateAudioCodecs()
    End Sub
    Private Sub CheckBoxH263_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxH263.CheckedChanged
        UpdateVideoCodecs()
    End Sub

    Private Sub CheckBoxH2631998_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxH2631998.CheckedChanged
        UpdateVideoCodecs()
    End Sub

    Private Sub CheckBoxH264_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxH264.CheckedChanged
        UpdateVideoCodecs()
    End Sub

    Private Sub CheckBoxAEC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxAEC.CheckedChanged

        If SIPInited = False Then
            Exit Sub
        End If

        If CheckBoxAEC.Checked = True Then
            AxPortSIPCoreLib1.enableAEC(1)
        Else
            AxPortSIPCoreLib1.enableAEC(0)
        End If
    End Sub

    Private Sub CheckBoxVAD_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxVAD.CheckedChanged

        If SIPInited = False Then
            Exit Sub
        End If

        If CheckBoxVAD.Checked = True Then
            AxPortSIPCoreLib1.enableVAD(1)
        Else
            AxPortSIPCoreLib1.enableVAD(0)
        End If
    End Sub

    Private Sub CheckBoxCNG_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxCNG.CheckedChanged
        If SIPInited = False Then
            Exit Sub
        End If


        If CheckBoxCNG.Checked = True Then
            AxPortSIPCoreLib1.enableCNG(1)
        Else
            AxPortSIPCoreLib1.enableCNG(0)
        End If
    End Sub

    Private Sub CheckBoxAGC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxAGC.CheckedChanged
        If SIPInited = False Then
            Exit Sub
        End If


        If CheckBoxAGC.Checked = True Then
            AxPortSIPCoreLib1.enableAGC(1)
        Else
            AxPortSIPCoreLib1.enableAGC(0)
        End If
    End Sub

    Private Sub Button17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button17.Click
        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            TextBoxRecordFilePath.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub Button19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button19.Click
        Try
            If SIPInited = False Then
                log_maintain("Please initialize the SDK first.")
                Exit Sub
            End If


            If TextBoxRecordFilePath.Text.Length <= 0 Or TextBoxAudioRecordFileName.Text.Length <= 0 Then
                log_maintain("The file path or file name is empty.")
                Exit Sub
            End If


            ' Record audio to file as WAVE format
            Dim AudioFileFormat As Long = FILEFORMAT_MP3

            ' If want to record the audio as ogg format, then use this:
            ' Dim AudioFilrFormat As Long = FILEFORMAT_OGG

            ' Set record file path and name
            Dim appendTimestamp As Long = 0
            If (CheckBoxTimestamp.Checked) Then
                appendTimestamp = 1
            End If
            AxPortSIPCoreLib1.setAudioRecordPathName(TextBoxRecordFilePath.Text, TextBoxAudioRecordFileName.Text, appendTimestamp, AudioFileFormat)

            ' Start recording
            AxPortSIPCoreLib1.startAudioRecording()

            log_maintain("Started recording audio conversation.")
        Catch ex As Exception
            log_maintain("Not Started recording audio conversation. Exception: " + ex.Message)
        End Try
        
    End Sub

    Private Sub Button18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button18.Click
        Try
            If SIPInited = False Then
                log_maintain("Please initialize the SDK first.")
                Exit Sub
            End If

            AxPortSIPCoreLib1.stopAudioRecording()

            log_maintain("Stop recording audio conversation.")
        Catch ex As Exception
            log_maintain("Not Stop recording audio conversation.Exception: " + ex.Message)
        End Try
        
    End Sub

    Private Sub Button20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button20.Click
        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            TextBoxPlayFile.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub Button21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button21.Click
        If SIPInited = False Then
            MsgBox("Please initialize the SDK first.")
            Exit Sub
        End If


        If TextBoxPlayFile.Text.Length <= 0 Then
            MsgBox("The play file is empty.")
            Exit Sub
        End If

        AxPortSIPCoreLib1.setPlayWaveFileToRemote(TextBoxPlayFile.Text, 1, 0, 0)
    End Sub

    Private Sub Button23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button23.Click
        If SIPInited = False Then
            MsgBox("Please initialize the SDK first.")
            Exit Sub
        End If

        AxPortSIPCoreLib1.setPlayWaveFileToRemote(TextBoxPlayFile.Text, 0, 0, 0)
    End Sub

    Private Sub AxPortSIPCoreLib1_ACTVTransferFailure(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_ACTVTransferFailureEvent) Handles AxPortSIPCoreLib1.ACTVTransferFailure
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If


        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Transfer failed"
        log_maintain(Text)

        ' e.reason ' e.reason is error reason
        ' e.code() ' e.code is error code

    End Sub

    Private Sub AxPortSIPCoreLib1_ACTVTransferSuccess(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_ACTVTransferSuccessEvent) Handles AxPortSIPCoreLib1.ACTVTransferSuccess
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If



        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Transfer succeeded, closed the call"
        log_maintain(Text)

        ' The ACTIVE Transfer success, then reset currently call.

        CallSessions(i).Reset()
    End Sub

    Private Sub AxPortSIPCoreLib1_arrivedSignaling(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_arrivedSignalingEvent) Handles AxPortSIPCoreLib1.arrivedSignaling
        'MsgBox(e.signaling)
        ' This event will be fired when the SDK received a message, the e.signaling is SIP message
        'log_SipMsg("SessionID:" & e.sessionId & "-Signal:" & e.signaling)
        log_maintain("SIP Message: " & e.signaling)
    End Sub

    Private Sub AxPortSIPCoreLib1_inviteAnswered(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_inviteAnsweredEvent) Handles AxPortSIPCoreLib1.inviteAnswered

        Dim i As Long = 0
        Dim state As Boolean = False

        If e.hasVideo = 1 Then
            log_maintain("This  call has video with codec: " & e.videoCodecName)
            ' This  call has video 
        ElseIf e.hasVideo = 0 Then
            log_maintain("This  call hasn't video")

            ' This call hasn't the video 
        End If

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If

        CallSessions(i).SetSessionState(True)
        CurrentlyLine = i
        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Call established"
        AxWindowsMediaPlayer1.URL = ""
        'AxPortSIPCoreLib1.playLocalWaveFile("")
        videoenabled()
        log_maintain(Text)
    End Sub

    Private Sub AxPortSIPCoreLib1_inviteClosed(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_inviteClosedEvent) Handles AxPortSIPCoreLib1.inviteClosed
        'AxWindowsMediaPlayer1.URL = ""
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If
        'MsgBox(e.ToString())
        CurrentlyLine = i
        CallSessions(i).Reset()
        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Call closed"
        AxWindowsMediaPlayer1.URL = ""
        'AxPortSIPCoreLib1.playLocalWaveFile("")
        log_maintain(Text)
        videodisabled()
    End Sub

    Private Sub AxPortSIPCoreLib1_inviteFailure(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_inviteFailureEvent) Handles AxPortSIPCoreLib1.inviteFailure

        'AxWindowsMediaPlayer1.URL = ""
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If

        CallSessions(i).Reset()


        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Call failed, "
        Text = Text + e.reason
        log_maintain(Text)
        AxWindowsMediaPlayer1.URL = ""
        videoenabledstatus = False
        'AxPortSIPCoreLib1.playLocalWaveFile("")
        ' the error reson is e.reason
        'the error code is e.code
    End Sub

    Private Sub AxPortSIPCoreLib1_inviteIncoming(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_inviteIncomingEvent) Handles AxPortSIPCoreLib1.inviteIncoming
        Try

            'NotifyIcon1.ShowBalloonTip(5, "CSI Phone", "Call Incoming from " & e.caller, ToolTipIcon.Info)
            log_maintain("specific message: ringing file playing")
            'AxWindowsMediaPlayer1.settings.autoStart = True
            'AxWindowsMediaPlayer1.settings.playCount = 100
            'AxWindowsMediaPlayer1.URL = Application.StartupPath & "\Whisper.wav" ' System.Configuration.ConfigurationSettings.AppSettings("ringpath")
            log_maintain("specific message: ringing file played")
        Catch ex As Exception
            log_maintain("specific message: ringing file playing error: " & ex.Message)
        End Try
        Dim i As Long = 0
        Dim state As Boolean = False

        If e.hasVideo = 1 Then
            log_maintain("This  call has video with codec: " & e.videoCodecName)
            ' This  call has video 
        ElseIf e.hasVideo = 0 Then
            log_maintain("This  call hasn't video")

            ' This call hasn't the video 
        End If

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionState() = False And CallSessions(i).GetRecvCallState() = False Then
                state = True
                CallSessions(i).SetRecvCallState(True)
                Exit For
            End If
        Next

        If state = False Then
            AxPortSIPCoreLib1.rejectCall(e.sessionId, 486, "Busy here")
            Exit Sub
        End If


        Dim Text As String = ""

        ' For DND(Do not disturb)
        If CheckBoxDND.Checked = True Then
            AxPortSIPCoreLib1.rejectCall(e.sessionId, 486, "Busy here")
            CallSessions(i).Reset()

            Text = "Line " + CStr(i)
            Text = Text + ": Reject the call by DND"
            log_maintain(Text)

            Exit Sub
        End If


        CallSessions(i).SetSessionId(e.sessionId)

        Dim needIgnoreAutoAnswer As Boolean = False
        Dim j As Long = 0

        'For j = LINE_BASE To MAX_LINES
        '    If CallSessions(j).GetSessionState() = True Then
        '        needIgnoreAutoAnswer = True
        '        Exit For
        '    End If
        'Next

        ' For AA(Auto answer)
        log_maintain(e.caller & " is calling " & e.callee)
        If needIgnoreAutoAnswer = False And CheckBoxAA.Checked = True Then
            CallSessions(i).SetRecvCallState(False)
            CallSessions(i).SetSessionState(True)

            AxPortSIPCoreLib1.answerCall(CallSessions(i).GetSessionId())
            CurrentlyLine = i
            Text = "Line " + CStr(i)
            Text = Text + ": Answered call by Auto Answer"
            log_maintain(Text)
            videoenabled()
            Exit Sub
        End If


        Text = "Line " + CStr(i)
        Text = Text + ": Call incoming from "
        Text = Text + e.callerDisplayName
        Text = Text + " <"
        Text = Text + e.caller
        Text = Text + ">"
        log_maintain(Text)

        ' Need to play the wave file here to alert the incoming call
    End Sub

    Private Sub AxPortSIPCoreLib1_inviteRinging(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_inviteRingingEvent) Handles AxPortSIPCoreLib1.inviteRinging
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If


        If e.hasEarlyMedia = 0 Then
            ' Not has the early media, you must play the local WAVE  file for ringing tone
            ' play the wav file for ring tone
        End If

        If e.hasVideo = 1 Then
            ' if e.hasEarlyMedia is 1, and e.hasVideo is 1, then means this call has early media and video.
        Else
            ' Does not has video
        End If



        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Ringing..."
        log_maintain(Text)
    End Sub

    Private Sub AxPortSIPCoreLib1_inviteTrying(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_inviteTryingEvent) Handles AxPortSIPCoreLib1.inviteTrying
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If

        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Call is trying..."
        log_maintain(Text)
    End Sub

    Private Sub AxPortSIPCoreLib1_inviteUpdated(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_inviteUpdatedEvent) Handles AxPortSIPCoreLib1.inviteUpdated
        'AxWindowsMediaPlayer1.URL = ""
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If

        CurrentlyLine = i
        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Call is updated..."
        log_maintain(Text)

        '   e.hasVideo
        '   e.audioCodecName
        '   e.videoCodecName


    End Sub

    Private Sub AxPortSIPCoreLib1_PASVTransferFailure(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_PASVTransferFailureEvent) Handles AxPortSIPCoreLib1.PASVTransferFailure
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If

        ' e.reason ' e.reason is error reason
        ' e.code() ' e.code is error code

        CurrentlyLine = i
        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Transfer failed"
        log_maintain(Text)
    End Sub

    Private Sub AxPortSIPCoreLib1_PASVTransferSuccess(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_PASVTransferSuccessEvent) Handles AxPortSIPCoreLib1.PASVTransferSuccess
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If

        CurrentlyLine = i
        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Transfer succeeded"
        log_maintain(Text)
    End Sub

    Private Sub AxPortSIPCoreLib1_recvPagerMessage(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_recvPagerMessageEvent) Handles AxPortSIPCoreLib1.recvPagerMessage
        Dim Message As String = "Received SIP pager message from "
        Message = Message + e.from
        Message = Message + " :"
        Message = Message + e.message

        MsgBox(Message)
    End Sub

    Private Sub AxPortSIPCoreLib1_receivedDtmfTone(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_receivedDtmfToneEvent) Handles AxPortSIPCoreLib1.receivedDtmfTone
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If


        Dim DtmfTone As String = e.tone.ToString()
        If e.tone = "10" Then
            DtmfTone = "*"
        End If

        If e.tone = "11" Then
            DtmfTone = "#"
        End If

        Dim Line As String = e.sessionId

        Dim Text As String = "Received DTMF tone: "
        Text = Text + DtmfTone
        Text = Text + " on line "
        Text = Text + i.ToString()

        log_maintain(Text)

    End Sub


    Private Sub AxPortSIPCoreLib1_registerFailure(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_registerFailureEvent) Handles AxPortSIPCoreLib1.registerFailure
        log_maintain("Failure Reason-" & e.reason)
        log_maintain("Register failure")
        Me.NotifyIcon1.ShowBalloonTip(5, "Sip Phone", "Register failure", ToolTipIcon.Info)
        SIPLogined = False
        Sip_Unregister()
        Timer1.Enabled = True
    End Sub

    Private Sub AxPortSIPCoreLib1_registerSuccess(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AxPortSIPCoreLib1.registerSuccess
        log_maintain("Registration succeeded at Extn: " & TextBoxUserName.Text & " on " & ComboBoxLines.SelectedItem.ToString)
        Me.Text = "CSI SIP Phone V " & Application.ProductVersion & " - " & username
        Me.NotifyIcon1.Text = "Sip Phone registered on " & username
        Me.NotifyIcon1.ShowBalloonTip(5, "Sip Phone", "Sip Phone registered on " & username, ToolTipIcon.Info)
        'Me.Hide()
        'Me.WindowState = FormWindowState.Minimized
        'Me.NotifyIcon1.Visible = True
        videodisabled()
        If ComboBoxSpeakers.Items.Count > 0 Then
            log_maintain("Speaker selected: " & ComboBoxSpeakers.SelectedItem.ToString)
        Else
            log_maintain("Speaker Not Found")
        End If
        If ComboBoxMicrophones.Items.Count > 0 Then
            log_maintain("Microphone selected: " & ComboBoxMicrophones.SelectedItem.ToString)
        Else
            log_maintain("Microphone Not Found")
        End If
        SIPLogined = True
    End Sub

    Private Sub AxPortSIPCoreLib1_inviteBeginForward(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_inviteBeginForwardEvent) Handles AxPortSIPCoreLib1.inviteBeginForward

        Dim Text As String = "The call has been forward to: "
        Text = Text + e.forwardingTo
        log_maintain(Text)
    End Sub

    Private Sub AxPortSIPCoreLib1_remoteHold(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_remoteHoldEvent) Handles AxPortSIPCoreLib1.remoteHold
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If


        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Placed on hold by remote party"
        log_maintain(Text)
    End Sub

    Private Sub AxPortSIPCoreLib1_remoteUnHold(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_remoteUnHoldEvent) Handles AxPortSIPCoreLib1.remoteUnHold
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If

        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Take off hold by remote party"
        log_maintain(Text)
    End Sub

    Private Sub AxPortSIPCoreLib1_sendPagerMessageFailure(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_sendPagerMessageFailureEvent) Handles AxPortSIPCoreLib1.sendPagerMessageFailure
        log_maintain("Send pager message failure")

        ' e.callee ' this is the message sender
        ' e.caller ' this is the message receiver

        ' e.reason ' e.reason is error reason
        ' e.code() ' e.code is error code
    End Sub

    Private Sub AxPortSIPCoreLib1_sendPagerMessageSuccess(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_sendPagerMessageSuccessEvent) Handles AxPortSIPCoreLib1.sendPagerMessageSuccess
        log_maintain("Send pager message succeeded")

        ' e.callee ' this is the message sender
        ' e.caller ' this is the message receiver
    End Sub

    Private Sub AxPortSIPCoreLib1_transferRinging(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_transferRingingEvent) Handles AxPortSIPCoreLib1.transferRinging
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If e.sessionId = CallSessions(i).GetSessionId() Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If


        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Transfer Ringing"
        log_maintain(Text)

        ' use e.hasVideo to check does this transfer call has video.
        ' if e.hasVideo is 1, then it's have video, if e.hasVideo is 0, means does not has video.
    End Sub

    Private Sub AxPortSIPCoreLib1_transferTrying(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_transferTryingEvent) Handles AxPortSIPCoreLib1.transferTrying
        Dim i As Long = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If e.sessionId = CallSessions(i).GetSessionId() Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If


        ' for example, if A and B is established call, A transfer B to C, the transfer is trying,
        ' B will got this transferTring event, and use e.referTo to know C ( C is "e.referTo" in this case)


        Dim Text As String = "Line " + CStr(i)
        Text = Text + ": Transfer Trying"
        log_maintain(Text)
    End Sub

    Private Sub AxPortSIPCoreLib1_waitingFaxMessage(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_waitingFaxMessageEvent) Handles AxPortSIPCoreLib1.waitingFaxMessage
        Dim Text As String = e.messageAccount
        Text = Text + " has FAX message"
        log_maintain(Text)

        ' These four parameters for message count/
        '     e.newMessageCount
        '    e.oldMessageCount
        '    e.urgentNewMessageCount
        '   e.urgentOldMessageCount

    End Sub

    Private Sub AxPortSIPCoreLib1_waitingVoiceMessage(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_waitingVoiceMessageEvent) Handles AxPortSIPCoreLib1.waitingVoiceMessage
        Dim Text As String = e.messageAccount
        Text = Text + " has voice message"
        log_maintain(Text)

        ' These four parameters for message count/
        '     e.newMessageCount
        '    e.oldMessageCount
        '    e.urgentNewMessageCount
        '   e.urgentOldMessageCount

    End Sub


    Private Sub Button24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button24.Click
        If SIPInited = False Or SIPLogined = False Then
            Exit Sub
        End If

        If CallSessions(CurrentlyLine).GetSessionState() = False Then
            MsgBox("Need to make the call established first")
            Exit Sub
        End If


        If Form2.ShowDialog() <> DialogResult.OK Then
            Exit Sub
        End If


        If Form2.GetTransferNumber().Length <= 0 Then
            MsgBox("The transfer number is empty")
            Exit Sub
        End If


        Dim replaceLine = Form2.GetLineNum()
        If replaceLine <= 0 Or replaceLine > MAX_LINES Then
            MsgBox("The replace line out of range")
            Exit Sub
        End If


        If CallSessions(replaceLine).GetSessionState() = False Then
            MsgBox("The replace line does not established yet")
            Exit Sub
        End If



        Dim sessionId As Long = CallSessions(CurrentlyLine).GetSessionId()
        Dim replaceSessionId As Long = CallSessions(replaceLine).GetSessionId()

        Dim ReferTo As String = Form2.GetTransferNumber()


        Dim Text As String


        Text = ""


        If AxPortSIPCoreLib1.attendedRefer(sessionId, replaceSessionId, ReferTo) = -1 Then
            Text = "Line " + CStr(CurrentlyLine)
            Text = Text + ": Transfer failed"
            log_maintain(Text)

            Exit Sub
        End If


        Text = "Line " + CStr(CurrentlyLine)
        Text = Text + ": Transfering"
        log_maintain(Text)
    End Sub

    Private Sub ComboBoxSpeakers_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxSpeakers.SelectedIndexChanged
        If SIPInited = True Then
            AxPortSIPCoreLib1.setAudioDeviceId(ComboBoxMicrophones.SelectedIndex, ComboBoxSpeakers.SelectedIndex)
            log_maintain("Speaker selected: " & ComboBoxSpeakers.SelectedItem.ToString)
        End If
    End Sub

    Private Sub ComboBoxMicrophones_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxMicrophones.SelectedIndexChanged
        If SIPInited = True Then
            AxPortSIPCoreLib1.setAudioDeviceId(ComboBoxMicrophones.SelectedIndex, ComboBoxSpeakers.SelectedIndex)
            log_maintain("Microphone selected: " & ComboBoxMicrophones.SelectedItem.ToString)
        End If
    End Sub

    Private Sub ComboBoxCameras_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxCameras.SelectedIndexChanged
        If SIPInited = True Then
            AxPortSIPCoreLib1.setVideoDeviceId(ComboBoxCameras.SelectedIndex)
        End If
    End Sub


    Private Sub Button26_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button26.Click
        If SIPInited = False Then
            MsgBox("Please initialize the SDK first.")
            Exit Sub
        End If


        If TextBoxRecordFilePath.Text.Length <= 0 Or TextBoxVideoRecordFileName.Text.Length <= 0 Then
            MsgBox("The file path or file name is empty.")
            Exit Sub
        End If


        ' Record remote video as AVI file
        Dim mode As Long = VIDEOINFO_ONLY_REMOTE

        ' If you want to record local video, then use:
        ' Dim mode As Long = VIDEOINFO_ONLY_LOCAL


        ' If you want to record both of local and remote video, then use:
        ' Dim mode As Long = VIDEOINFO_ONLY_BOTH

        Dim appendTimestamp As Long = 0
        If (CheckBoxTimestamp.Checked) Then
            appendTimestamp = 1
        End If

        ' Set record file path and name
        AxPortSIPCoreLib1.setVideoRecordPathName(TextBoxRecordFilePath.Text, TextBoxVideoRecordFileName.Text, appendTimestamp, mode)

        ' Start recording
        AxPortSIPCoreLib1.startVideoRecording()

        MsgBox("Start recording video conversation.")
    End Sub

    Private Sub Button25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button25.Click
        If SIPInited = False Then
            MsgBox("Please initialize the SDK first.")
            Exit Sub
        End If

        AxPortSIPCoreLib1.stopVideoRecording()

        MsgBox("Stop recording video conversation.")
    End Sub

    Private Sub CheckBoxAudioStream_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxAudioStream.CheckedChanged
        If SIPInited = False Then
            MsgBox("Please initialize the SDK first.")
            Exit Sub
        End If

        If CheckBoxAudioStream.Checked = True Then

            ' Callback remote audio stream
            AxPortSIPCoreLib1.enableAudioStreamCallback(AUDIOSTREAM_REMOTE)

            ' Callback local audio stream
            ' AxPortSIPCoreLib1.enableAudioStreamCallback(AUDIOSTREAM_LOCAL)

            ' Callback local and remote audio both
            ' AxPortSIPCoreLib1.enableAudioStreamCallback(AUDIOSTREAM_BOTH)

        Else

            ' Disable audio stream callback
            AxPortSIPCoreLib1.enableAudioStreamCallback(AUDIOSTREAM_NONE)
        End If

    End Sub

    Private Sub AxPortSIPCoreLib1_receivedAudioStream(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_receivedAudioStreamEvent) Handles AxPortSIPCoreLib1.receivedAudioStream
        ' You can access the audio stream callback data here, when the enableAudioStreamCallback is called
        ' then this event will be fired when received each audio RTP packet

        ' e.audioStream is audio stream callback data, it's VARIANT type, PCM, 16bit, 8KHz, Mono
        ' e.streamType is the audio stream type

        If e.streamType = AUDIOSTREAM_LOCAL Then
            ' This is the audio stream from local sound card captured.
        ElseIf e.streamType = AUDIOSTREAM_REMOTE Then
            '  This is the audio stream which received from remote side
        ElseIf e.streamType = AUDIOSTREAM_LOCAL_REMOTE_MIX Then
            ' This is audio stream which mixed of local sound card captured audio and received remote audio
        End If

    End Sub


    Private Sub AxPortSIPCoreLib1_receivedVideoStream(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_receivedVideoStreamEvent) Handles AxPortSIPCoreLib1.receivedVideoStream
        ' If called the enableAudioStreamCallback, then this event will be fired if received each video RTP packet
        ' or captured local video from camera.

        ' e.sessionId  ' The call session which video stream from
        ' e.height ' Video image height
        ' e.width ' Video image width
        ' e.videoStream ' Video stream data, VARIANT type, RGB888 format.

        Dim i As Long = 0
        Dim state As Boolean = False

        If e.sessionId = 0 Then
            ' If the sessionId is 0, means this is the local video stream
        Else
            For i = LINE_BASE To MAX_LINES
                If CallSessions(i).GetSessionId() = e.sessionId Then
                    state = True
                    Exit For
                End If
            Next
        End If

    End Sub



    Private Sub Button27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button27.Click
        If SIPInited = False Then
            MsgBox("Please initialize the SDK first.")
            Exit Sub
        End If

        If ((TextBoxForwardTo.Text.IndexOf("sip:") = -1) Or (TextBoxForwardTo.Text.IndexOf("@") = -1)) Then
            MsgBox("The forward address must likes sip:xxxxx@sip.portsip.com")
            Exit Sub
        End If

        If CheckBoxForwardCallBusy.Checked = True Then
            AxPortSIPCoreLib1.enableCallForwarding(1, TextBoxForwardTo.Text)
        Else
            AxPortSIPCoreLib1.enableCallForwarding(0, TextBoxForwardTo.Text)
        End If


        MsgBox("Call forward is enabled.")

    End Sub

    Private Sub Button28_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button28.Click
        If SIPInited = False Then
            MsgBox("Please initialize the SDK first.")
            Exit Sub
        End If

        AxPortSIPCoreLib1.disableCallForwarding()

        MsgBox("Call forward is disabled")
    End Sub


    Private Sub ComboBoxSRTP_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxSRTP.SelectedIndexChanged
        SetSRTPType()
    End Sub


    Private Sub ComboBoxVideoResolution_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxVideoResolution.SelectedIndexChanged
        SetVideoResolution()
    End Sub



    Private Sub TrackBarVideoQuality_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBarVideoQuality.Scroll
        SetVideoQuality()
    End Sub


    Private Sub AxPortSIPCoreLib1_receivedInfo(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_receivedInfoEvent) Handles AxPortSIPCoreLib1.receivedInfo

        Dim i As Integer = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If

        Dim Text As String = "Received a INFO message on line "
        Text += CStr(i)
        Text += ": "
        Text += e.infoMessage

        MsgBox(Text)

    End Sub


    Private Sub AxPortSIPCoreLib1_receivedMessage(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_receivedMessageEvent) Handles AxPortSIPCoreLib1.receivedMessage
        Dim i As Integer = 0
        Dim state As Boolean = False

        For i = LINE_BASE To MAX_LINES
            If CallSessions(i).GetSessionId() = e.sessionId Then
                state = True
                Exit For
            End If
        Next

        If state = False Then
            Exit Sub
        End If

        Dim Text As String = "Received a MESSAGE message on line "
        Text += CStr(i)
        Text += ": "
        Text += e.message


        MsgBox(Text)
    End Sub


    Private Sub AxPortSIPCoreLib1_receivedOptions(ByVal sender As System.Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_receivedOptionsEvent) Handles AxPortSIPCoreLib1.receivedOptions
        Dim Text As String = "Received an OPTIONS message: "
        Text += e.optionsMessage

        'MsgBox(Text)

    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        System.Diagnostics.Process.Start("http://portsip.com")
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        System.Diagnostics.Process.Start("mailto:sales@portsip.com")
    End Sub


    Private Sub TextBoxUserName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxUserName.TextChanged
        'TextBoxPassword.Text = TextBoxUserName.Text
        TextBoxDisplayName.Text = TextBoxUserName.Text
        TextBoxAuthName.Text = TextBoxUserName.Text
        username = TextBoxUserName.Text
    End Sub

    Private Sub TextBoxServer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxServer.TextChanged
        TextBoxUserDomain.Text = TextBoxServer.Text
    End Sub

    Private Sub ShowToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowToolStripMenuItem.Click
        Me.Width = 738
        Me.Height = 346
        PictureBox3.Left = 678
        PictureBox3.Top = 24

    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Width = 408
        Me.Height = 346
        PictureBox3.Left = 353
        PictureBox3.Top = 24
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub ShowToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowToolStripMenuItem1.Click
        If Me.WindowState = FormWindowState.Minimized Then
            Me.Show()
            Me.WindowState = FormWindowState.Normal
        End If
    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        'If Me.WindowState = FormWindowState.Minimized Then
        Me.Show()
        Me.WindowState = FormWindowState.Normal
        Me.NotifyIcon1.Visible = False
        'End If
    End Sub

    Private Sub ToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem2.Click
        Me.Close()
    End Sub


    Private Sub NotifyIcon1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseUp
        Dim pt As System.Drawing.Point
        pt.X = Cursor.Position.X
        pt.Y = Cursor.Position.Y - 20
        ContextMenuStrip1.Show(pt, ToolStripDropDownDirection.AboveRight)

    End Sub

    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        frmAbout.Show()
    End Sub

    Private Sub AboutToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem1.Click
        frmAbout.Show()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim DeviceCount As Integer = 0
        Dim i As Integer = 0
        Dim DeviceName As String = ""
        DeviceCount = AxDeviceManagerLib1.getAudioOutputDeviceNums()
        If DeviceCount = 0 Then
            If cntDeviceCount = DeviceCount And initialstage <> "initial" Then
                Exit Sub
            Else
                initialstage = ""
                log_maintain("Device Not Found")
                log_maintain("Registration Failed due to No Audio Devices found")
                log_maintain("Phone Unregistered ...")
                ComboBoxSpeakers.Items.Clear()
                ComboBoxMicrophones.Items.Clear()
                ComboBoxCameras.Items.Clear()
                Me.NotifyIcon1.ShowBalloonTip(5, "Sip Phone", "Registration Failed due to No Audio Devices found", ToolTipIcon.Info)
                Me.NotifyIcon1.Text = "Registration Failed due to No Audio Devices found"
            End If
            cntDeviceCount = 0
            Button2_Click(sender, e)
        End If
        If DeviceCount > 0 Then
            If cntDeviceCount = DeviceCount And str_msg <> "Register failure" Then
                Exit Sub
            Else
                log_maintain("Device Found")
                Timer1.Enabled = False
                Me.NotifyIcon1.ShowBalloonTip(5, "Sip Phone", "Device Found", ToolTipIcon.Info)
            End If
            ComboBoxSpeakers.Items.Clear()
            ComboBoxMicrophones.Items.Clear()
            ComboBoxCameras.Items.Clear()
            cntDeviceCount = DeviceCount
            For i = 0 To DeviceCount - 1
                AxDeviceManagerLib1.getAudioOutputDeviceName(i, DeviceName)
                ComboBoxSpeakers.Items.Add(DeviceName)
            Next

            If ComboBoxSpeakers.Items.Count > 0 Then
                ComboBoxSpeakers.SelectedIndex = 0
            End If
            DeviceCount = AxDeviceManagerLib1.getAudioInputDeviceNums()
            For i = 0 To DeviceCount - 1
                AxDeviceManagerLib1.getAudioInputDeviceName(i, DeviceName)
                ComboBoxMicrophones.Items.Add(DeviceName)
            Next

            If ComboBoxMicrophones.Items.Count > 0 Then
                ComboBoxMicrophones.SelectedIndex = 0
            End If
            DeviceCount = AxDeviceManagerLib1.getVideoDeviceNums()
            For i = 0 To DeviceCount - 1
                AxDeviceManagerLib1.getVideoDeviceName(i, DeviceName)
                ComboBoxCameras.Items.Add(DeviceName)
            Next

            If ComboBoxCameras.Items.Count > 0 Then
                ComboBoxCameras.SelectedIndex = 0
            End If
            Button1_Click(sender, e)
        End If
    End Sub
    Dim cntDeviceCount As Integer = 0
    Dim initialstage As String = "initial"
    Public str_msg As String
    Public Function log_maintain(ByVal str As String) As String
        str_msg = str
        ListBoxSIPLog.Items.Add(str)
        log_maintain = ""
        LogMaintain(str)
    End Function

    Private Sub KeyPadToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KeyPadToolStripMenuItem1.Click
        frmKeypad.Top = Windows.Forms.Cursor.Position.Y - frmKeypad.Height
        frmKeypad.Left = Windows.Forms.Cursor.Position.X - frmKeypad.Width
        frmKeypad.Show()
    End Sub
    Private Sub ListBoxSIPLog_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBoxSIPLog.DoubleClick
        ToolTip1.Show(ListBoxSIPLog.Items(ListBoxSIPLog.SelectedIndex).ToString, ListBoxSIPLog, 10000)
    End Sub

    Private Sub Subscribe_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Subscribe.Tick
        Dim ret As Integer
        ret = AxPortSIPCoreLib1.presenceSubscribeContact("sip:" & TextBoxServer.Text & "@" & TextBoxServer.Text, TextBoxUserName.Text)
        If ret = 0 Then
            LogMaintain("subscribe success")
        Else
            LogMaintain("subscribe failure")
        End If
    End Sub
    Dim subscribeid
    Private Sub AxPortSIPCoreLib1_presenceRecvSubscribe(ByVal sender As Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_presenceRecvSubscribeEvent) Handles AxPortSIPCoreLib1.presenceRecvSubscribe
        Dim str As String
        str = "presenceRecvSubscribe- subject id: " & e.subscribeId & ". subject: " & e.subject & ". from: " & e.from & ". fromDisplayName: " & e.fromDisplayName
        AxPortSIPCoreLib1.presenceAcceptSubscribe(e.subscribeId)
        LogMaintain(str)
    End Sub

    Private Sub AxPortSIPCoreLib1_presenceContactOnline(ByVal sender As Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_presenceContactOnlineEvent) Handles AxPortSIPCoreLib1.presenceContactOnline
        Dim str As String
        str = "presenceContactOnline- statetext: " & e.stateText & ". from: " & e.from & ". fromDisplayName: " & e.fromDisplayName
        LogMaintain(str)
    End Sub

    Private Sub AxPortSIPCoreLib1_presenceContactOffline(ByVal sender As Object, ByVal e As AxPortSIPCoreLibLib._DPortSIPCoreLibEvents_presenceContactOfflineEvent) Handles AxPortSIPCoreLib1.presenceContactOffline
        Dim str As String
        str = "presenceContactOffline- from: " & e.from & ". fromDisplayName: " & e.fromDisplayName
        LogMaintain(str)
    End Sub
    Private Sub ListenerInititialize()
        Try
            Dim prt As String
            prt = System.Configuration.ConfigurationSettings.AppSettings("listenaudioport")
            Dim localAddr As IPAddress = IPAddress.Parse("127.0.0.1")
            serverSocket = New TcpListener(localAddr, prt)
            serverSocket.Start()
abc:
            clientSocket = serverSocket.AcceptTcpClient()
            While (True)
                Try
                    Dim ns As NetworkStream = clientSocket.GetStream()
                    Dim bytesFrom(1024) As Byte
                    ns.Read(bytesFrom, 0, bytesFrom.Length)
                    Dim dataFromClient As String = System.Text.Encoding.ASCII.GetString(bytesFrom)
                    dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"))
                    functionality(dataFromClient)
                    Dim serverResponse As String = dataFromClient & "$"
                    Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes(serverResponse)
                    ns.Write(sendBytes, 0, sendBytes.Length)
                    ns.Flush()
                Catch ex As Exception
                    LogMaintain("Exception occured-" & ex.Message.ToString)
                    GoTo abc
                End Try
            End While
            clientSocket.Close()
            serverSocket.Stop()
        Catch ex As Exception
            LogMaintain("Exception occured2-" & ex.Message.ToString)
        End Try
    End Sub
    Private Sub ListenerInititialize2()
        Try
            Dim prt As String
            prt = System.Configuration.ConfigurationSettings.AppSettings("listenvideoport")
            Dim localAddr As IPAddress = IPAddress.Parse("127.0.0.1")
            serverSocket2 = New TcpListener(localAddr, prt)
            serverSocket2.Start()
abc:
            clientSocket2 = serverSocket2.AcceptTcpClient()
            While (True)
                Try
                    Dim ns As NetworkStream = clientSocket2.GetStream()
                    Dim bytesFrom(1024) As Byte
                    ns.Read(bytesFrom, 0, bytesFrom.Length)
                    Dim dataFromClient As String = System.Text.Encoding.ASCII.GetString(bytesFrom)
                    dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"))
                    functionality2(dataFromClient)
                    Dim serverResponse As String = dataFromClient & "$"
                    Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes(serverResponse)
                    ns.Write(sendBytes, 0, sendBytes.Length)
                    ns.Flush()
                Catch ex As Exception
                    LogMaintain("Exception occured-" & ex.Message.ToString)
                    GoTo abc
                End Try
            End While
            clientSocket2.Close()
            serverSocket2.Stop()
        Catch ex As Exception
            LogMaintain("Exception occured2-" & ex.Message.ToString)
        End Try
    End Sub
    Dim vtask As String
    Dim vtaskvideo As String
    Dim vrecpath As String
    Dim vrecfile As String
    Dim vcallid As String
    Dim vextn As String
    Dim vmdbpath As String
    Sub functionality(ByVal msg As String)
        Try
            Dim ret
            ret = Split(msg, "|", -1, CompareMethod.Text)
            LogMaintain("msg received: " & msg)
            Dim ret2
            vtask = ""
            'vcallid = ""
            'vrecfile = ""
            'vrecpath = ""
            For i As Integer = LBound(ret) To UBound(ret)
                ret2 = Split(ret(i).ToString, "-", -1, CompareMethod.Text)
                Select Case ret2(0)
                    ''Case "mdbpath"
                    ''    vmdbpath = ret2(1)
                    ''    LogMaintain("mdbpath: " & msg)
                    ''Case "callid"
                    ''    vcallid = ret2(1)
                    ''    LogMaintain("callid: " & vcallid)
                    ''Case "recfile"
                    ''    vrecfile = ret2(1)
                    ''    LogMaintain("recfile: " & vrecfile)
                    ''Case "recpath"
                    ''    vrecpath = ret2(1)
                    ''    LogMaintain("recpath: " & vrecpath)
                    Case "task"
                        vtask = ret2(1)
                        LogMaintain("task: " & vtask)
                End Select
            Next

        Catch ex As Exception
            LogMaintain("Exception occured-" & msg & "-" & ex.InnerException.Message.ToString)
        End Try
    End Sub
    Sub functionality2(ByVal msg As String)
        Try
            Dim ret
            ret = Split(msg, "|", -1, CompareMethod.Text)
            LogMaintain("msg received: " & msg)
            Dim ret2
            vtask = ""
            'vcallid = ""
            'vrecfile = ""
            'vrecpath = ""
            For i As Integer = LBound(ret) To UBound(ret)
                ret2 = Split(ret(i).ToString, "-", -1, CompareMethod.Text)
                Select Case ret2(0)
                    ''Case "mdbpath"
                    ''    vmdbpath = ret2(1)
                    ''    LogMaintain("mdbpath: " & msg)
                    ''Case "callid"
                    ''    vcallid = ret2(1)
                    ''    LogMaintain("callid: " & vcallid)
                    ''Case "recfile"
                    ''    vrecfile = ret2(1)
                    ''    LogMaintain("recfile: " & vrecfile)
                    ''Case "recpath"
                    ''    vrecpath = ret2(1)
                    ''    LogMaintain("recpath: " & vrecpath)
                    Case "task"
                        vtaskvideo = ret2(1)
                        LogMaintain("task: " & vtask)
                End Select
            Next

        Catch ex As Exception
            LogMaintain("Exception occured-" & msg & "-" & ex.InnerException.Message.ToString)
        End Try
    End Sub
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Timer2.Enabled = False
        'task:start|callid:1|recpath:1|recfile:1|mdbpath:1$
        Dim query As String = ""
        If vtask = "start" Then
            vtask = ""
            DialToQueue()
            'vrecpath = vrecpath.Substring(0, Len(vrecpath) - 1)
            'LogMaintain("changed vrecpath: " & vrecpath)
            'TextBoxRecordFilePath.Text = vrecpath
            'TextBoxAudioRecordFileName.Text = vrecfile
            'Button19_Click(sender, e)
        ElseIf vtask = "stop" Then
            vtask = ""
            QueueEnd()
            'TextBoxRecordFilePath.Text = ""
            'TextBoxAudioRecordFileName.Text = ""
            'Button18_Click(sender, e)
        ElseIf vtaskvideo = "start" Then
            vtask = ""
            DialToQueue()
            'vrecpath = vrecpath.Substring(0, Len(vrecpath) - 1)
            'LogMaintain("changed vrecpath: " & vrecpath)
            'TextBoxRecordFilePath.Text = vrecpath
            'TextBoxAudioRecordFileName.Text = vrecfile
            'Button19_Click(sender, e)
        ElseIf vtaskvideo = "stop" Then
            vtask = ""
            QueueEnd()
            'TextBoxRecordFilePath.Text = ""
            'TextBoxAudioRecordFileName.Text = ""
            'Button18_Click(sender, e)
        End If
        Timer2.Enabled = True
    End Sub

    Private Sub ButtonTestVideo_Click(sender As Object, e As EventArgs) Handles ButtonTestVideo.Click

        If ButtonTestVideo.Text = "Test Video" Then
            AxDeviceManagerLib1.startVideoPreview(BigVideo.Handle, ComboBoxCameras.SelectedIndex, 0, 0, BigVideo.Width, BigVideo.Height)
            SmallVideo.Visible = False
            ButtonTestVideo.Text = "Stop Test"
        Else
            AxDeviceManagerLib1.stopVideoPreview()
            SmallVideo.Visible = True
            ButtonTestVideo.Text = "Test Video"
        End If
    End Sub

    Private Sub GroupBox3_Enter(sender As Object, e As EventArgs) Handles GroupBox3.Enter

    End Sub
End Class


