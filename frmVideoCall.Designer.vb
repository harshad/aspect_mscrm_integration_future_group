﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVideoCall
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVideoCall))
        Me.btnConnect = New System.Windows.Forms.Button()
        Me.btnConnect2 = New System.Windows.Forms.Button()
        Me.btnEndCall = New System.Windows.Forms.Button()
        Me.btnEndCall2 = New System.Windows.Forms.Button()
        Me.wmpADV = New AxWMPLib.AxWindowsMediaPlayer()
        CType(Me.wmpADV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnConnect
        '
        Me.btnConnect.BackColor = System.Drawing.Color.Transparent
        Me.btnConnect.BackgroundImage = CType(resources.GetObject("btnConnect.BackgroundImage"), System.Drawing.Image)
        Me.btnConnect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnConnect.FlatAppearance.BorderSize = 0
        Me.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConnect.Location = New System.Drawing.Point(754, 917)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(194, 79)
        Me.btnConnect.TabIndex = 9
        Me.btnConnect.TabStop = False
        Me.btnConnect.UseVisualStyleBackColor = False
        '
        'btnConnect2
        '
        Me.btnConnect2.BackColor = System.Drawing.Color.Transparent
        Me.btnConnect2.BackgroundImage = CType(resources.GetObject("btnConnect2.BackgroundImage"), System.Drawing.Image)
        Me.btnConnect2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnConnect2.FlatAppearance.BorderSize = 0
        Me.btnConnect2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConnect2.Location = New System.Drawing.Point(754, 919)
        Me.btnConnect2.Name = "btnConnect2"
        Me.btnConnect2.Size = New System.Drawing.Size(194, 79)
        Me.btnConnect2.TabIndex = 10
        Me.btnConnect2.TabStop = False
        Me.btnConnect2.UseVisualStyleBackColor = False
        '
        'btnEndCall
        '
        Me.btnEndCall.BackColor = System.Drawing.Color.Transparent
        Me.btnEndCall.BackgroundImage = CType(resources.GetObject("btnEndCall.BackgroundImage"), System.Drawing.Image)
        Me.btnEndCall.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnEndCall.FlatAppearance.BorderSize = 0
        Me.btnEndCall.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEndCall.Location = New System.Drawing.Point(969, 917)
        Me.btnEndCall.Name = "btnEndCall"
        Me.btnEndCall.Size = New System.Drawing.Size(194, 79)
        Me.btnEndCall.TabIndex = 11
        Me.btnEndCall.TabStop = False
        Me.btnEndCall.UseVisualStyleBackColor = False
        '
        'btnEndCall2
        '
        Me.btnEndCall2.BackColor = System.Drawing.Color.Transparent
        Me.btnEndCall2.BackgroundImage = CType(resources.GetObject("btnEndCall2.BackgroundImage"), System.Drawing.Image)
        Me.btnEndCall2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnEndCall2.FlatAppearance.BorderSize = 0
        Me.btnEndCall2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEndCall2.Location = New System.Drawing.Point(969, 919)
        Me.btnEndCall2.Name = "btnEndCall2"
        Me.btnEndCall2.Size = New System.Drawing.Size(194, 79)
        Me.btnEndCall2.TabIndex = 12
        Me.btnEndCall2.TabStop = False
        Me.btnEndCall2.UseVisualStyleBackColor = False
        '
        'wmpADV
        '
        Me.wmpADV.Enabled = True
        Me.wmpADV.Location = New System.Drawing.Point(510, 283)
        Me.wmpADV.Margin = New System.Windows.Forms.Padding(2)
        Me.wmpADV.Name = "wmpADV"
        Me.wmpADV.OcxState = CType(resources.GetObject("wmpADV.OcxState"), System.Windows.Forms.AxHost.State)
        Me.wmpADV.Size = New System.Drawing.Size(900, 600)
        Me.wmpADV.TabIndex = 8
        '
        'frmVideoCall
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1920, 1080)
        Me.Controls.Add(Me.btnEndCall)
        Me.Controls.Add(Me.btnConnect)
        Me.Controls.Add(Me.wmpADV)
        Me.Controls.Add(Me.btnConnect2)
        Me.Controls.Add(Me.btnEndCall2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmVideoCall"
        Me.Text = "Video Call"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.wmpADV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents wmpADV As AxWMPLib.AxWindowsMediaPlayer
    Private WithEvents btnConnect As Button
    Private WithEvents btnConnect2 As Button
    Private WithEvents btnEndCall As Button
    Private WithEvents btnEndCall2 As Button
End Class
