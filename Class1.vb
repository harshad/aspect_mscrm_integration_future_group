Public Class Sessions
    Private mSessionId As Long = 0
    Private mHoldState As Boolean = False
    Private mSessionState As Boolean = False
    Private mRecvCallState As Boolean = False


    Public Sub SetSessionId(ByVal sessionId As Long)
        mSessionId = sessionId
    End Sub

    Public Function GetSessionId() As Long
        Return mSessionId
    End Function

    Public Sub SetSessionState(ByVal sessionState As Boolean)
        mSessionState = sessionState
    End Sub


    Public Function GetSessionState() As Boolean
        Return mSessionState
    End Function

    Public Sub SetRecvCallState(ByVal recvState As Boolean)
        mRecvCallState = recvState
    End Sub

    Public Function GetRecvCallState() As Boolean
        Return mRecvCallState
    End Function



    Public Sub SetHoldState(ByVal holdState As Boolean)
        mHoldState = holdState
    End Sub

    Public Function GetHoldState() As Boolean
        Return mHoldState
    End Function

    Public Sub Reset()
        mSessionState = False
        mRecvCallState = False
        mHoldState = False
        mSessionId = 0
    End Sub


End Class
