<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmKeypad
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.lblCLose = New System.Windows.Forms.Label
        Me.btn1 = New System.Windows.Forms.Button
        Me.btnHash = New System.Windows.Forms.Button
        Me.btn0 = New System.Windows.Forms.Button
        Me.btnStar = New System.Windows.Forms.Button
        Me.btn9 = New System.Windows.Forms.Button
        Me.btn8 = New System.Windows.Forms.Button
        Me.btn7 = New System.Windows.Forms.Button
        Me.btn6 = New System.Windows.Forms.Button
        Me.btn5 = New System.Windows.Forms.Button
        Me.btn4 = New System.Windows.Forms.Button
        Me.btn3 = New System.Windows.Forms.Button
        Me.btn2 = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.White
        Me.GroupBox3.Controls.Add(Me.btn1)
        Me.GroupBox3.Controls.Add(Me.btnHash)
        Me.GroupBox3.Controls.Add(Me.btn0)
        Me.GroupBox3.Controls.Add(Me.btnStar)
        Me.GroupBox3.Controls.Add(Me.btn9)
        Me.GroupBox3.Controls.Add(Me.btn8)
        Me.GroupBox3.Controls.Add(Me.btn7)
        Me.GroupBox3.Controls.Add(Me.btn6)
        Me.GroupBox3.Controls.Add(Me.btn5)
        Me.GroupBox3.Controls.Add(Me.btn4)
        Me.GroupBox3.Controls.Add(Me.btn3)
        Me.GroupBox3.Controls.Add(Me.btn2)
        Me.GroupBox3.Location = New System.Drawing.Point(2, 16)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(154, 116)
        Me.GroupBox3.TabIndex = 85
        Me.GroupBox3.TabStop = False
        '
        'lblCLose
        '
        Me.lblCLose.AutoSize = True
        Me.lblCLose.BackColor = System.Drawing.Color.Red
        Me.lblCLose.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCLose.ForeColor = System.Drawing.Color.White
        Me.lblCLose.Location = New System.Drawing.Point(142, 3)
        Me.lblCLose.Name = "lblCLose"
        Me.lblCLose.Size = New System.Drawing.Size(13, 13)
        Me.lblCLose.TabIndex = 30
        Me.lblCLose.Text = "x"
        '
        'btn1
        '
        Me.btn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn1.Location = New System.Drawing.Point(4, 19)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(45, 22)
        Me.btn1.TabIndex = 18
        Me.btn1.Text = "1"
        Me.btn1.UseVisualStyleBackColor = False
        '
        'btnHash
        '
        Me.btnHash.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnHash.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnHash.Location = New System.Drawing.Point(104, 88)
        Me.btnHash.Name = "btnHash"
        Me.btnHash.Size = New System.Drawing.Size(45, 22)
        Me.btnHash.TabIndex = 29
        Me.btnHash.Text = "#"
        Me.btnHash.UseVisualStyleBackColor = False
        '
        'btn0
        '
        Me.btn0.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn0.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn0.Location = New System.Drawing.Point(54, 88)
        Me.btn0.Name = "btn0"
        Me.btn0.Size = New System.Drawing.Size(45, 22)
        Me.btn0.TabIndex = 28
        Me.btn0.Text = "0"
        Me.btn0.UseVisualStyleBackColor = False
        '
        'btnStar
        '
        Me.btnStar.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnStar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStar.Location = New System.Drawing.Point(4, 88)
        Me.btnStar.Name = "btnStar"
        Me.btnStar.Size = New System.Drawing.Size(45, 22)
        Me.btnStar.TabIndex = 27
        Me.btnStar.Text = "*"
        Me.btnStar.UseVisualStyleBackColor = False
        '
        'btn9
        '
        Me.btn9.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn9.Location = New System.Drawing.Point(104, 65)
        Me.btn9.Name = "btn9"
        Me.btn9.Size = New System.Drawing.Size(45, 22)
        Me.btn9.TabIndex = 26
        Me.btn9.Text = "9"
        Me.btn9.UseVisualStyleBackColor = False
        '
        'btn8
        '
        Me.btn8.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn8.Location = New System.Drawing.Point(54, 65)
        Me.btn8.Name = "btn8"
        Me.btn8.Size = New System.Drawing.Size(45, 22)
        Me.btn8.TabIndex = 25
        Me.btn8.Text = "8"
        Me.btn8.UseVisualStyleBackColor = False
        '
        'btn7
        '
        Me.btn7.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn7.Location = New System.Drawing.Point(4, 65)
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(45, 22)
        Me.btn7.TabIndex = 24
        Me.btn7.Text = "7"
        Me.btn7.UseVisualStyleBackColor = False
        '
        'btn6
        '
        Me.btn6.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn6.Location = New System.Drawing.Point(104, 42)
        Me.btn6.Name = "btn6"
        Me.btn6.Size = New System.Drawing.Size(45, 22)
        Me.btn6.TabIndex = 23
        Me.btn6.Text = "6"
        Me.btn6.UseVisualStyleBackColor = False
        '
        'btn5
        '
        Me.btn5.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn5.Location = New System.Drawing.Point(54, 42)
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(45, 22)
        Me.btn5.TabIndex = 22
        Me.btn5.Text = "5"
        Me.btn5.UseVisualStyleBackColor = False
        '
        'btn4
        '
        Me.btn4.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn4.Location = New System.Drawing.Point(4, 42)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(45, 22)
        Me.btn4.TabIndex = 21
        Me.btn4.Text = "4"
        Me.btn4.UseVisualStyleBackColor = False
        '
        'btn3
        '
        Me.btn3.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn3.Location = New System.Drawing.Point(104, 19)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(45, 22)
        Me.btn3.TabIndex = 20
        Me.btn3.Text = "3"
        Me.btn3.UseVisualStyleBackColor = False
        '
        'btn2
        '
        Me.btn2.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn2.Location = New System.Drawing.Point(54, 19)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(45, 22)
        Me.btn2.TabIndex = 19
        Me.btn2.Text = "2"
        Me.btn2.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Gray
        Me.Panel1.Controls.Add(Me.lblCLose)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(155, 20)
        Me.Panel1.TabIndex = 86
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 87
        Me.Label1.Tag = ""
        Me.Label1.Text = "CSI Dial Pad"
        '
        'frmKeypad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gray
        Me.ClientSize = New System.Drawing.Size(159, 134)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmKeypad"
        Me.Text = "CSi Keypad"
        Me.GroupBox3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btn1 As System.Windows.Forms.Button
    Friend WithEvents btnHash As System.Windows.Forms.Button
    Friend WithEvents btn0 As System.Windows.Forms.Button
    Friend WithEvents btnStar As System.Windows.Forms.Button
    Friend WithEvents btn9 As System.Windows.Forms.Button
    Friend WithEvents btn8 As System.Windows.Forms.Button
    Friend WithEvents btn7 As System.Windows.Forms.Button
    Friend WithEvents btn6 As System.Windows.Forms.Button
    Friend WithEvents btn5 As System.Windows.Forms.Button
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents btn3 As System.Windows.Forms.Button
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents lblCLose As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
