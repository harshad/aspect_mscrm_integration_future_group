<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DisplayWindow
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DisplayWindow))
        Me.SmallVideo = New System.Windows.Forms.PictureBox
        Me.BigVideo = New System.Windows.Forms.PictureBox
        CType(Me.SmallVideo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BigVideo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SmallVideo
        '
        Me.SmallVideo.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SmallVideo.BackgroundImage = CType(resources.GetObject("SmallVideo.BackgroundImage"), System.Drawing.Image)
        Me.SmallVideo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SmallVideo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SmallVideo.ErrorImage = Nothing
        Me.SmallVideo.InitialImage = Nothing
        Me.SmallVideo.Location = New System.Drawing.Point(278, 239)
        Me.SmallVideo.Name = "SmallVideo"
        Me.SmallVideo.Size = New System.Drawing.Size(124, 124)
        Me.SmallVideo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SmallVideo.TabIndex = 9
        Me.SmallVideo.TabStop = False
        '
        'BigVideo
        '
        Me.BigVideo.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BigVideo.BackgroundImage = CType(resources.GetObject("BigVideo.BackgroundImage"), System.Drawing.Image)
        Me.BigVideo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BigVideo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.BigVideo.ErrorImage = Nothing
        Me.BigVideo.InitialImage = Nothing
        Me.BigVideo.Location = New System.Drawing.Point(0, -1)
        Me.BigVideo.Name = "BigVideo"
        Me.BigVideo.Size = New System.Drawing.Size(402, 364)
        Me.BigVideo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.BigVideo.TabIndex = 8
        Me.BigVideo.TabStop = False
        '
        'DisplayWindow
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(402, 363)
        Me.Controls.Add(Me.SmallVideo)
        Me.Controls.Add(Me.BigVideo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "DisplayWindow"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        CType(Me.SmallVideo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BigVideo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SmallVideo As System.Windows.Forms.PictureBox
    Friend WithEvents BigVideo As System.Windows.Forms.PictureBox
End Class
