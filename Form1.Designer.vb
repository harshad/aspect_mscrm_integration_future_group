<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.CheckBoxConf = New System.Windows.Forms.CheckBox()
        Me.CheckBoxAA = New System.Windows.Forms.CheckBox()
        Me.CheckBoxDND = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.CheckBoxSDP = New System.Windows.Forms.CheckBox()
        Me.TextBoxRecordFilePath = New System.Windows.Forms.TextBox()
        Me.ButtonAnswer = New System.Windows.Forms.Button()
        Me.ButtonTransfer = New System.Windows.Forms.Button()
        Me.ButtonHold = New System.Windows.Forms.Button()
        Me.ComboBoxLines = New System.Windows.Forms.ComboBox()
        Me.ButtonReject = New System.Windows.Forms.Button()
        Me.ButtonHangUp = New System.Windows.Forms.Button()
        Me.ButtonDial = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TextBoxPhoneNumber = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBoxStunPort = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBoxStunServer = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBoxServerPort = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxServer = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBoxPassword = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxUserName = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.ButtonSendVideo = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.CheckBoxH2631998 = New System.Windows.Forms.CheckBox()
        Me.ButtonLocalVideo = New System.Windows.Forms.Button()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxTimestamp = New System.Windows.Forms.CheckBox()
        Me.CheckBoxAudioStream = New System.Windows.Forms.CheckBox()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.TextBoxVideoRecordFileName = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.TextBoxAudioRecordFileName = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.ButtonCameraOptions = New System.Windows.Forms.Button()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.ButtonVideoPreview = New System.Windows.Forms.Button()
        Me.ComboBoxMicrophones = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.CheckBoxAGC = New System.Windows.Forms.CheckBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.CheckBoxCNG = New System.Windows.Forms.CheckBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.CheckBoxVAD = New System.Windows.Forms.CheckBox()
        Me.CheckBoxAEC = New System.Windows.Forms.CheckBox()
        Me.ComboBoxSpeakers = New System.Windows.Forms.ComboBox()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.ComboBoxCameras = New System.Windows.Forms.ComboBox()
        Me.CheckBoxH264 = New System.Windows.Forms.CheckBox()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxG723 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxGSM = New System.Windows.Forms.CheckBox()
        Me.CheckBoxH263 = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TextBoxUserDomain = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBoxAuthName = New System.Windows.Forms.TextBox()
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.AxDeviceManagerLib1 = New AxDeviceManagerLibLib.AxDeviceManagerLib()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBoxDisplayName = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.ComboBoxTransport = New System.Windows.Forms.ComboBox()
        Me.ComboBoxSRTP = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.CheckBoxG7221 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxSpeexWB = New System.Windows.Forms.CheckBox()
        Me.CheckBoxAMRwb = New System.Windows.Forms.CheckBox()
        Me.CheckBoxSpeex = New System.Windows.Forms.CheckBox()
        Me.CheckBoxG722 = New System.Windows.Forms.CheckBox()
        Me.CheckBoxILBC = New System.Windows.Forms.CheckBox()
        Me.CheckBoxG729 = New System.Windows.Forms.CheckBox()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.CheckBoxPCMA = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPCMU = New System.Windows.Forms.CheckBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.TrackBarVideoQuality = New System.Windows.Forms.TrackBar()
        Me.ComboBoxVideoResolution = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.ListBoxSIPLog = New System.Windows.Forms.ListBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox16 = New System.Windows.Forms.GroupBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TrackBarSpeaker = New System.Windows.Forms.TrackBar()
        Me.ButtonTestAudio = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TrackBarMicrophone = New System.Windows.Forms.TrackBar()
        Me.CheckBoxMute = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.ButtonTestVideo = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.GroupBox15 = New System.Windows.Forms.GroupBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.TextBoxPlayFile = New System.Windows.Forms.TextBox()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Button27 = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.CheckBoxForwardCallBusy = New System.Windows.Forms.CheckBox()
        Me.TextBoxForwardTo = New System.Windows.Forms.TextBox()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.GroupBox14 = New System.Windows.Forms.GroupBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.KeyPadToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Subscribe = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.SmallVideo = New System.Windows.Forms.PictureBox()
        Me.BigVideo = New System.Windows.Forms.PictureBox()
        Me.AxWindowsMediaPlayer1 = New AxWMPLib.AxWindowsMediaPlayer()
        Me.AxPortSIPCoreLib1 = New AxPortSIPCoreLibLib.AxPortSIPCoreLib()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.AxDeviceManagerLib1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBarVideoQuality, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        CType(Me.TrackBarSpeaker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBarMicrophone, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SmallVideo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BigVideo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxWindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxPortSIPCoreLib1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CheckBoxConf
        '
        Me.CheckBoxConf.AutoSize = True
        Me.CheckBoxConf.Location = New System.Drawing.Point(442, 20)
        Me.CheckBoxConf.Name = "CheckBoxConf"
        Me.CheckBoxConf.Size = New System.Drawing.Size(81, 17)
        Me.CheckBoxConf.TabIndex = 44
        Me.CheckBoxConf.Text = "Conference"
        Me.CheckBoxConf.UseVisualStyleBackColor = True
        '
        'CheckBoxAA
        '
        Me.CheckBoxAA.AutoSize = True
        Me.CheckBoxAA.Checked = True
        Me.CheckBoxAA.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxAA.Location = New System.Drawing.Point(62, 17)
        Me.CheckBoxAA.Name = "CheckBoxAA"
        Me.CheckBoxAA.Size = New System.Drawing.Size(86, 17)
        Me.CheckBoxAA.TabIndex = 43
        Me.CheckBoxAA.Text = "Auto Answer"
        Me.CheckBoxAA.UseVisualStyleBackColor = True
        '
        'CheckBoxDND
        '
        Me.CheckBoxDND.AutoSize = True
        Me.CheckBoxDND.Location = New System.Drawing.Point(305, 17)
        Me.CheckBoxDND.Name = "CheckBoxDND"
        Me.CheckBoxDND.Size = New System.Drawing.Size(122, 17)
        Me.CheckBoxDND.TabIndex = 42
        Me.CheckBoxDND.Text = "Do not disturb(DND)"
        Me.CheckBoxDND.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(17, 152)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(425, 3)
        Me.GroupBox2.TabIndex = 41
        Me.GroupBox2.TabStop = False
        '
        'Button22
        '
        Me.Button22.Location = New System.Drawing.Point(330, 212)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(87, 23)
        Me.Button22.TabIndex = 40
        Me.Button22.Text = "Clear"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'CheckBoxSDP
        '
        Me.CheckBoxSDP.AutoSize = True
        Me.CheckBoxSDP.Location = New System.Drawing.Point(154, 17)
        Me.CheckBoxSDP.Name = "CheckBoxSDP"
        Me.CheckBoxSDP.Size = New System.Drawing.Size(134, 17)
        Me.CheckBoxSDP.TabIndex = 38
        Me.CheckBoxSDP.Text = "Make call without SDP"
        Me.CheckBoxSDP.UseVisualStyleBackColor = True
        '
        'TextBoxRecordFilePath
        '
        Me.TextBoxRecordFilePath.Location = New System.Drawing.Point(8, 40)
        Me.TextBoxRecordFilePath.Name = "TextBoxRecordFilePath"
        Me.TextBoxRecordFilePath.ReadOnly = True
        Me.TextBoxRecordFilePath.Size = New System.Drawing.Size(403, 20)
        Me.TextBoxRecordFilePath.TabIndex = 1
        '
        'ButtonAnswer
        '
        Me.ButtonAnswer.Location = New System.Drawing.Point(471, 290)
        Me.ButtonAnswer.Name = "ButtonAnswer"
        Me.ButtonAnswer.Size = New System.Drawing.Size(60, 23)
        Me.ButtonAnswer.TabIndex = 36
        Me.ButtonAnswer.Text = "Answer"
        Me.ButtonAnswer.UseVisualStyleBackColor = True
        '
        'ButtonTransfer
        '
        Me.ButtonTransfer.Location = New System.Drawing.Point(680, 274)
        Me.ButtonTransfer.Name = "ButtonTransfer"
        Me.ButtonTransfer.Size = New System.Drawing.Size(87, 23)
        Me.ButtonTransfer.TabIndex = 35
        Me.ButtonTransfer.Text = "Transfer"
        Me.ButtonTransfer.UseVisualStyleBackColor = True
        '
        'ButtonHold
        '
        Me.ButtonHold.Location = New System.Drawing.Point(613, 304)
        Me.ButtonHold.Name = "ButtonHold"
        Me.ButtonHold.Size = New System.Drawing.Size(60, 23)
        Me.ButtonHold.TabIndex = 34
        Me.ButtonHold.Text = "Hold"
        Me.ButtonHold.UseVisualStyleBackColor = True
        '
        'ComboBoxLines
        '
        Me.ComboBoxLines.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxLines.FormattingEnabled = True
        Me.ComboBoxLines.Location = New System.Drawing.Point(4, 121)
        Me.ComboBoxLines.Name = "ComboBoxLines"
        Me.ComboBoxLines.Size = New System.Drawing.Size(183, 21)
        Me.ComboBoxLines.TabIndex = 33
        '
        'ButtonReject
        '
        Me.ButtonReject.Location = New System.Drawing.Point(773, 274)
        Me.ButtonReject.Name = "ButtonReject"
        Me.ButtonReject.Size = New System.Drawing.Size(60, 23)
        Me.ButtonReject.TabIndex = 32
        Me.ButtonReject.Text = "Reject"
        Me.ButtonReject.UseVisualStyleBackColor = True
        '
        'ButtonHangUp
        '
        Me.ButtonHangUp.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonHangUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonHangUp.Location = New System.Drawing.Point(202, 10)
        Me.ButtonHangUp.Name = "ButtonHangUp"
        Me.ButtonHangUp.Size = New System.Drawing.Size(44, 23)
        Me.ButtonHangUp.TabIndex = 31
        Me.ButtonHangUp.Text = "Hang Up"
        Me.ButtonHangUp.UseVisualStyleBackColor = False
        '
        'ButtonDial
        '
        Me.ButtonDial.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonDial.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonDial.Location = New System.Drawing.Point(155, 10)
        Me.ButtonDial.Name = "ButtonDial"
        Me.ButtonDial.Size = New System.Drawing.Size(44, 23)
        Me.ButtonDial.TabIndex = 30
        Me.ButtonDial.Text = "Dial"
        Me.ButtonDial.UseVisualStyleBackColor = False
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button12.Location = New System.Drawing.Point(105, 157)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(45, 22)
        Me.Button12.TabIndex = 29
        Me.Button12.Text = "#"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button13.Location = New System.Drawing.Point(55, 157)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(45, 22)
        Me.Button13.TabIndex = 28
        Me.Button13.Text = "0"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'Button14
        '
        Me.Button14.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button14.Location = New System.Drawing.Point(5, 157)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(45, 22)
        Me.Button14.TabIndex = 27
        Me.Button14.Text = "*"
        Me.Button14.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Location = New System.Drawing.Point(105, 134)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(45, 22)
        Me.Button9.TabIndex = 26
        Me.Button9.Text = "9"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Location = New System.Drawing.Point(55, 134)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(45, 22)
        Me.Button10.TabIndex = 25
        Me.Button10.Text = "8"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Location = New System.Drawing.Point(5, 134)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(45, 22)
        Me.Button11.TabIndex = 24
        Me.Button11.Text = "7"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Location = New System.Drawing.Point(105, 111)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(45, 22)
        Me.Button6.TabIndex = 23
        Me.Button6.Text = "6"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Location = New System.Drawing.Point(55, 111)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(45, 22)
        Me.Button7.TabIndex = 22
        Me.Button7.Text = "5"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Location = New System.Drawing.Point(5, 111)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(45, 22)
        Me.Button8.TabIndex = 21
        Me.Button8.Text = "4"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Location = New System.Drawing.Point(105, 88)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(45, 22)
        Me.Button5.TabIndex = 20
        Me.Button5.Text = "3"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Location = New System.Drawing.Point(55, 88)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(45, 22)
        Me.Button4.TabIndex = 19
        Me.Button4.Text = "2"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.DefaultExt = "wav"
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Title = "Select wave file"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(5, 88)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(45, 22)
        Me.Button3.TabIndex = 18
        Me.Button3.Text = "1"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'TextBoxPhoneNumber
        '
        Me.TextBoxPhoneNumber.Location = New System.Drawing.Point(52, 12)
        Me.TextBoxPhoneNumber.Name = "TextBoxPhoneNumber"
        Me.TextBoxPhoneNumber.Size = New System.Drawing.Size(92, 20)
        Me.TextBoxPhoneNumber.TabIndex = 17
        Me.TextBoxPhoneNumber.Text = "8000"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(225, 50)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(66, 21)
        Me.Button2.TabIndex = 12
        Me.Button2.Text = "Offline"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(225, 23)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(66, 21)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "Online"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'TextBoxStunPort
        '
        Me.TextBoxStunPort.Location = New System.Drawing.Point(308, 95)
        Me.TextBoxStunPort.Name = "TextBoxStunPort"
        Me.TextBoxStunPort.Size = New System.Drawing.Size(132, 20)
        Me.TextBoxStunPort.TabIndex = 8
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(231, 99)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(26, 13)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Port"
        '
        'TextBoxStunServer
        '
        Me.TextBoxStunServer.Location = New System.Drawing.Point(83, 95)
        Me.TextBoxStunServer.Name = "TextBoxStunServer"
        Me.TextBoxStunServer.Size = New System.Drawing.Size(132, 20)
        Me.TextBoxStunServer.TabIndex = 7
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 99)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "Stun Server"
        '
        'TextBoxServerPort
        '
        Me.TextBoxServerPort.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxServerPort.ForeColor = System.Drawing.Color.Black
        Me.TextBoxServerPort.Location = New System.Drawing.Point(102, 62)
        Me.TextBoxServerPort.Name = "TextBoxServerPort"
        Me.TextBoxServerPort.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxServerPort.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(21, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 14)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Server Port"
        '
        'TextBoxServer
        '
        Me.TextBoxServer.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxServer.ForeColor = System.Drawing.Color.Black
        Me.TextBoxServer.Location = New System.Drawing.Point(102, 37)
        Me.TextBoxServer.Name = "TextBoxServer"
        Me.TextBoxServer.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxServer.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(21, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 14)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Server IP"
        '
        'TextBoxPassword
        '
        Me.TextBoxPassword.Location = New System.Drawing.Point(308, 12)
        Me.TextBoxPassword.Name = "TextBoxPassword"
        Me.TextBoxPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextBoxPassword.Size = New System.Drawing.Size(132, 20)
        Me.TextBoxPassword.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(231, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Password"
        '
        'TextBoxUserName
        '
        Me.TextBoxUserName.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxUserName.ForeColor = System.Drawing.Color.Black
        Me.TextBoxUserName.Location = New System.Drawing.Point(101, 11)
        Me.TextBoxUserName.Name = "TextBoxUserName"
        Me.TextBoxUserName.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxUserName.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(21, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "User Name"
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(337, 129)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(80, 23)
        Me.Button15.TabIndex = 50
        Me.Button15.Text = "Stop Video"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'ButtonSendVideo
        '
        Me.ButtonSendVideo.Location = New System.Drawing.Point(254, 129)
        Me.ButtonSendVideo.Name = "ButtonSendVideo"
        Me.ButtonSendVideo.Size = New System.Drawing.Size(80, 23)
        Me.ButtonSendVideo.TabIndex = 50
        Me.ButtonSendVideo.Text = "Send Video"
        Me.ButtonSendVideo.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(110, 16)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(69, 23)
        Me.Button17.TabIndex = 7
        Me.Button17.Text = "..."
        Me.Button17.UseVisualStyleBackColor = True
        '
        'CheckBoxH2631998
        '
        Me.CheckBoxH2631998.AutoSize = True
        Me.CheckBoxH2631998.Location = New System.Drawing.Point(627, 165)
        Me.CheckBoxH2631998.Name = "CheckBoxH2631998"
        Me.CheckBoxH2631998.Size = New System.Drawing.Size(79, 17)
        Me.CheckBoxH2631998.TabIndex = 62
        Me.CheckBoxH2631998.Text = "H263-1998"
        Me.CheckBoxH2631998.UseVisualStyleBackColor = True
        '
        'ButtonLocalVideo
        '
        Me.ButtonLocalVideo.Location = New System.Drawing.Point(171, 129)
        Me.ButtonLocalVideo.Name = "ButtonLocalVideo"
        Me.ButtonLocalVideo.Size = New System.Drawing.Size(80, 23)
        Me.ButtonLocalVideo.TabIndex = 49
        Me.ButtonLocalVideo.Text = "Local Video"
        Me.ButtonLocalVideo.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.Location = New System.Drawing.Point(124, 86)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(75, 25)
        Me.Button23.TabIndex = 12
        Me.Button23.Text = "Stop"
        Me.Button23.UseVisualStyleBackColor = True
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(217, 67)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(94, 23)
        Me.Button19.TabIndex = 6
        Me.Button19.Text = "Start Record"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.CheckBoxTimestamp)
        Me.GroupBox7.Controls.Add(Me.CheckBoxAudioStream)
        Me.GroupBox7.Controls.Add(Me.Button25)
        Me.GroupBox7.Controls.Add(Me.Button26)
        Me.GroupBox7.Controls.Add(Me.TextBoxVideoRecordFileName)
        Me.GroupBox7.Controls.Add(Me.Label16)
        Me.GroupBox7.Controls.Add(Me.Button17)
        Me.GroupBox7.Controls.Add(Me.Button18)
        Me.GroupBox7.Controls.Add(Me.Button19)
        Me.GroupBox7.Controls.Add(Me.TextBoxAudioRecordFileName)
        Me.GroupBox7.Controls.Add(Me.Label25)
        Me.GroupBox7.Controls.Add(Me.TextBoxRecordFilePath)
        Me.GroupBox7.Controls.Add(Me.Label26)
        Me.GroupBox7.Location = New System.Drawing.Point(924, 228)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(426, 99)
        Me.GroupBox7.TabIndex = 25
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Recording"
        '
        'CheckBoxTimestamp
        '
        Me.CheckBoxTimestamp.AutoSize = True
        Me.CheckBoxTimestamp.Location = New System.Drawing.Point(194, 19)
        Me.CheckBoxTimestamp.Name = "CheckBoxTimestamp"
        Me.CheckBoxTimestamp.Size = New System.Drawing.Size(77, 17)
        Me.CheckBoxTimestamp.TabIndex = 12
        Me.CheckBoxTimestamp.Text = "Timestamp"
        Me.CheckBoxTimestamp.UseVisualStyleBackColor = True
        Me.CheckBoxTimestamp.Visible = False
        '
        'CheckBoxAudioStream
        '
        Me.CheckBoxAudioStream.AutoSize = True
        Me.CheckBoxAudioStream.Location = New System.Drawing.Point(288, 19)
        Me.CheckBoxAudioStream.Name = "CheckBoxAudioStream"
        Me.CheckBoxAudioStream.Size = New System.Drawing.Size(130, 17)
        Me.CheckBoxAudioStream.TabIndex = 12
        Me.CheckBoxAudioStream.Text = "AudioStream Callback"
        Me.CheckBoxAudioStream.UseVisualStyleBackColor = True
        Me.CheckBoxAudioStream.Visible = False
        '
        'Button25
        '
        Me.Button25.Location = New System.Drawing.Point(317, 98)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(94, 23)
        Me.Button25.TabIndex = 11
        Me.Button25.Text = "Stop Record"
        Me.Button25.UseVisualStyleBackColor = True
        '
        'Button26
        '
        Me.Button26.Location = New System.Drawing.Point(217, 98)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(94, 23)
        Me.Button26.TabIndex = 10
        Me.Button26.Text = "Start Record"
        Me.Button26.UseVisualStyleBackColor = True
        '
        'TextBoxVideoRecordFileName
        '
        Me.TextBoxVideoRecordFileName.Location = New System.Drawing.Point(128, 100)
        Me.TextBoxVideoRecordFileName.Name = "TextBoxVideoRecordFileName"
        Me.TextBoxVideoRecordFileName.Size = New System.Drawing.Size(84, 20)
        Me.TextBoxVideoRecordFileName.TabIndex = 9
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 103)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(116, 13)
        Me.Label16.TabIndex = 8
        Me.Label16.Text = "Record video file name"
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(317, 67)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(94, 23)
        Me.Button18.TabIndex = 6
        Me.Button18.Text = "Stop Record"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'TextBoxAudioRecordFileName
        '
        Me.TextBoxAudioRecordFileName.Location = New System.Drawing.Point(128, 69)
        Me.TextBoxAudioRecordFileName.Name = "TextBoxAudioRecordFileName"
        Me.TextBoxAudioRecordFileName.Size = New System.Drawing.Size(84, 20)
        Me.TextBoxAudioRecordFileName.TabIndex = 3
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(6, 74)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(116, 13)
        Me.Label25.TabIndex = 2
        Me.Label25.Text = "Record audio file name"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(6, 22)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(101, 13)
        Me.Label26.TabIndex = 0
        Me.Label26.Text = "Record file directory"
        '
        'Button21
        '
        Me.Button21.Location = New System.Drawing.Point(16, 86)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(75, 25)
        Me.Button21.TabIndex = 11
        Me.Button21.Text = "Start"
        Me.Button21.UseVisualStyleBackColor = True
        '
        'ButtonCameraOptions
        '
        Me.ButtonCameraOptions.Location = New System.Drawing.Point(105, 129)
        Me.ButtonCameraOptions.Name = "ButtonCameraOptions"
        Me.ButtonCameraOptions.Size = New System.Drawing.Size(63, 23)
        Me.ButtonCameraOptions.TabIndex = 48
        Me.ButtonCameraOptions.Text = "Options"
        Me.ButtonCameraOptions.UseVisualStyleBackColor = True
        '
        'Button24
        '
        Me.Button24.Location = New System.Drawing.Point(680, 304)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(87, 23)
        Me.Button24.TabIndex = 69
        Me.Button24.Text = "Attend Trans"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'ButtonVideoPreview
        '
        Me.ButtonVideoPreview.Location = New System.Drawing.Point(8, 129)
        Me.ButtonVideoPreview.Name = "ButtonVideoPreview"
        Me.ButtonVideoPreview.Size = New System.Drawing.Size(94, 23)
        Me.ButtonVideoPreview.TabIndex = 47
        Me.ButtonVideoPreview.Text = "Video Preview"
        Me.ButtonVideoPreview.UseVisualStyleBackColor = True
        '
        'ComboBoxMicrophones
        '
        Me.ComboBoxMicrophones.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMicrophones.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxMicrophones.ForeColor = System.Drawing.Color.Black
        Me.ComboBoxMicrophones.FormattingEnabled = True
        Me.ComboBoxMicrophones.Location = New System.Drawing.Point(71, 141)
        Me.ComboBoxMicrophones.Name = "ComboBoxMicrophones"
        Me.ComboBoxMicrophones.Size = New System.Drawing.Size(169, 22)
        Me.ComboBoxMicrophones.TabIndex = 6
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 193)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(43, 13)
        Me.Label14.TabIndex = 7
        Me.Label14.Text = "Camera"
        '
        'CheckBoxAGC
        '
        Me.CheckBoxAGC.AutoSize = True
        Me.CheckBoxAGC.Location = New System.Drawing.Point(775, 394)
        Me.CheckBoxAGC.Name = "CheckBoxAGC"
        Me.CheckBoxAGC.Size = New System.Drawing.Size(48, 17)
        Me.CheckBoxAGC.TabIndex = 68
        Me.CheckBoxAGC.Text = "AGC"
        Me.CheckBoxAGC.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(976, 331)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(47, 13)
        Me.Label13.TabIndex = 3
        Me.Label13.Text = "Speaker"
        Me.Label13.Visible = False
        '
        'CheckBoxCNG
        '
        Me.CheckBoxCNG.AutoSize = True
        Me.CheckBoxCNG.Checked = True
        Me.CheckBoxCNG.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxCNG.Location = New System.Drawing.Point(673, 398)
        Me.CheckBoxCNG.Name = "CheckBoxCNG"
        Me.CheckBoxCNG.Size = New System.Drawing.Size(49, 17)
        Me.CheckBoxCNG.TabIndex = 67
        Me.CheckBoxCNG.Text = "CNG"
        Me.CheckBoxCNG.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(976, 318)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(63, 13)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Microphone"
        Me.Label12.Visible = False
        '
        'CheckBoxVAD
        '
        Me.CheckBoxVAD.AutoSize = True
        Me.CheckBoxVAD.Location = New System.Drawing.Point(575, 398)
        Me.CheckBoxVAD.Name = "CheckBoxVAD"
        Me.CheckBoxVAD.Size = New System.Drawing.Size(48, 17)
        Me.CheckBoxVAD.TabIndex = 66
        Me.CheckBoxVAD.Text = "VAD"
        Me.CheckBoxVAD.UseVisualStyleBackColor = True
        '
        'CheckBoxAEC
        '
        Me.CheckBoxAEC.AutoSize = True
        Me.CheckBoxAEC.Checked = True
        Me.CheckBoxAEC.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxAEC.Location = New System.Drawing.Point(475, 398)
        Me.CheckBoxAEC.Name = "CheckBoxAEC"
        Me.CheckBoxAEC.Size = New System.Drawing.Size(47, 17)
        Me.CheckBoxAEC.TabIndex = 65
        Me.CheckBoxAEC.Text = "AEC"
        Me.CheckBoxAEC.UseVisualStyleBackColor = True
        '
        'ComboBoxSpeakers
        '
        Me.ComboBoxSpeakers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxSpeakers.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxSpeakers.ForeColor = System.Drawing.Color.Black
        Me.ComboBoxSpeakers.FormattingEnabled = True
        Me.ComboBoxSpeakers.Location = New System.Drawing.Point(72, 112)
        Me.ComboBoxSpeakers.Name = "ComboBoxSpeakers"
        Me.ComboBoxSpeakers.Size = New System.Drawing.Size(168, 22)
        Me.ComboBoxSpeakers.TabIndex = 5
        '
        'GroupBox10
        '
        Me.GroupBox10.Location = New System.Drawing.Point(472, 186)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(400, 2)
        Me.GroupBox10.TabIndex = 64
        Me.GroupBox10.TabStop = False
        '
        'ComboBoxCameras
        '
        Me.ComboBoxCameras.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxCameras.FormattingEnabled = True
        Me.ComboBoxCameras.Location = New System.Drawing.Point(70, 190)
        Me.ComboBoxCameras.Name = "ComboBoxCameras"
        Me.ComboBoxCameras.Size = New System.Drawing.Size(170, 21)
        Me.ComboBoxCameras.TabIndex = 8
        '
        'CheckBoxH264
        '
        Me.CheckBoxH264.AutoSize = True
        Me.CheckBoxH264.Location = New System.Drawing.Point(772, 165)
        Me.CheckBoxH264.Name = "CheckBoxH264"
        Me.CheckBoxH264.Size = New System.Drawing.Size(52, 17)
        Me.CheckBoxH264.TabIndex = 63
        Me.CheckBoxH264.Text = "H264"
        Me.CheckBoxH264.UseVisualStyleBackColor = True
        '
        'GroupBox9
        '
        Me.GroupBox9.Location = New System.Drawing.Point(472, 151)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(400, 2)
        Me.GroupBox9.TabIndex = 60
        Me.GroupBox9.TabStop = False
        '
        'CheckBoxG723
        '
        Me.CheckBoxG723.AutoSize = True
        Me.CheckBoxG723.Checked = True
        Me.CheckBoxG723.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxG723.Location = New System.Drawing.Point(772, 64)
        Me.CheckBoxG723.Name = "CheckBoxG723"
        Me.CheckBoxG723.Size = New System.Drawing.Size(61, 17)
        Me.CheckBoxG723.TabIndex = 59
        Me.CheckBoxG723.Text = "G723.1"
        Me.CheckBoxG723.UseVisualStyleBackColor = True
        '
        'CheckBoxGSM
        '
        Me.CheckBoxGSM.AutoSize = True
        Me.CheckBoxGSM.Checked = True
        Me.CheckBoxGSM.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxGSM.Location = New System.Drawing.Point(627, 64)
        Me.CheckBoxGSM.Name = "CheckBoxGSM"
        Me.CheckBoxGSM.Size = New System.Drawing.Size(74, 17)
        Me.CheckBoxGSM.TabIndex = 58
        Me.CheckBoxGSM.Text = "GSM 6.10"
        Me.CheckBoxGSM.UseVisualStyleBackColor = True
        '
        'CheckBoxH263
        '
        Me.CheckBoxH263.AutoSize = True
        Me.CheckBoxH263.Checked = True
        Me.CheckBoxH263.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxH263.Location = New System.Drawing.Point(472, 165)
        Me.CheckBoxH263.Name = "CheckBoxH263"
        Me.CheckBoxH263.Size = New System.Drawing.Size(52, 17)
        Me.CheckBoxH263.TabIndex = 61
        Me.CheckBoxH263.Text = "H263"
        Me.CheckBoxH263.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox4)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.TextBoxUserDomain)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.TextBoxAuthName)
        Me.GroupBox1.Controls.Add(Me.GroupBox13)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.AxDeviceManagerLib1)
        Me.GroupBox1.Controls.Add(Me.Button22)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Button24)
        Me.GroupBox1.Controls.Add(Me.TextBoxDisplayName)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.ButtonAnswer)
        Me.GroupBox1.Controls.Add(Me.ComboBoxTransport)
        Me.GroupBox1.Controls.Add(Me.GroupBox7)
        Me.GroupBox1.Controls.Add(Me.GroupBox10)
        Me.GroupBox1.Controls.Add(Me.ComboBoxLines)
        Me.GroupBox1.Controls.Add(Me.ComboBoxSRTP)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.ButtonReject)
        Me.GroupBox1.Controls.Add(Me.CheckBoxH264)
        Me.GroupBox1.Controls.Add(Me.CheckBoxH2631998)
        Me.GroupBox1.Controls.Add(Me.ButtonHold)
        Me.GroupBox1.Controls.Add(Me.CheckBoxH263)
        Me.GroupBox1.Controls.Add(Me.GroupBox9)
        Me.GroupBox1.Controls.Add(Me.ButtonTransfer)
        Me.GroupBox1.Controls.Add(Me.CheckBoxG7221)
        Me.GroupBox1.Controls.Add(Me.CheckBoxG723)
        Me.GroupBox1.Controls.Add(Me.CheckBoxSpeexWB)
        Me.GroupBox1.Controls.Add(Me.CheckBoxAMRwb)
        Me.GroupBox1.Controls.Add(Me.CheckBoxSpeex)
        Me.GroupBox1.Controls.Add(Me.CheckBoxGSM)
        Me.GroupBox1.Controls.Add(Me.CheckBoxG722)
        Me.GroupBox1.Controls.Add(Me.CheckBoxILBC)
        Me.GroupBox1.Controls.Add(Me.CheckBoxG729)
        Me.GroupBox1.Controls.Add(Me.Button16)
        Me.GroupBox1.Controls.Add(Me.CheckBoxPCMA)
        Me.GroupBox1.Controls.Add(Me.CheckBoxPCMU)
        Me.GroupBox1.Controls.Add(Me.GroupBox6)
        Me.GroupBox1.Controls.Add(Me.GroupBox5)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.TextBoxStunPort)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.TextBoxStunServer)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.TextBoxPassword)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(2, 389)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1228, 461)
        Me.GroupBox1.TabIndex = 24
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "SIP"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Button15)
        Me.GroupBox4.Controls.Add(Me.ButtonSendVideo)
        Me.GroupBox4.Controls.Add(Me.ButtonLocalVideo)
        Me.GroupBox4.Controls.Add(Me.ButtonCameraOptions)
        Me.GroupBox4.Controls.Add(Me.ButtonVideoPreview)
        Me.GroupBox4.Location = New System.Drawing.Point(17, 16)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(423, 161)
        Me.GroupBox4.TabIndex = 46
        Me.GroupBox4.TabStop = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(373, 127)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(35, 13)
        Me.Label21.TabIndex = 54
        Me.Label21.Text = "Worst"
        '
        'TextBoxUserDomain
        '
        Me.TextBoxUserDomain.Location = New System.Drawing.Point(83, 53)
        Me.TextBoxUserDomain.Name = "TextBoxUserDomain"
        Me.TextBoxUserDomain.Size = New System.Drawing.Size(132, 20)
        Me.TextBoxUserDomain.TabIndex = 4
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(237, 126)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(28, 13)
        Me.Label20.TabIndex = 54
        Me.Label20.Text = "Best"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 57)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 13)
        Me.Label9.TabIndex = 83
        Me.Label9.Text = "User Domain"
        '
        'TextBoxAuthName
        '
        Me.TextBoxAuthName.Location = New System.Drawing.Point(308, 32)
        Me.TextBoxAuthName.Name = "TextBoxAuthName"
        Me.TextBoxAuthName.Size = New System.Drawing.Size(132, 20)
        Me.TextBoxAuthName.TabIndex = 3
        '
        'GroupBox13
        '
        Me.GroupBox13.Location = New System.Drawing.Point(113, 212)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(57, 39)
        Me.GroupBox13.TabIndex = 85
        Me.GroupBox13.TabStop = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(193, 124)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(39, 13)
        Me.Label19.TabIndex = 53
        Me.Label19.Text = "Quality"
        '
        'AxDeviceManagerLib1
        '
        Me.AxDeviceManagerLib1.Enabled = True
        Me.AxDeviceManagerLib1.Location = New System.Drawing.Point(234, 52)
        Me.AxDeviceManagerLib1.Name = "AxDeviceManagerLib1"
        Me.AxDeviceManagerLib1.OcxState = CType(resources.GetObject("AxDeviceManagerLib1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxDeviceManagerLib1.Size = New System.Drawing.Size(100, 50)
        Me.AxDeviceManagerLib1.TabIndex = 31
        Me.AxDeviceManagerLib1.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(291, 218)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(30, 13)
        Me.Label15.TabIndex = 52
        Me.Label15.Text = "Logs"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(231, 36)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 13)
        Me.Label5.TabIndex = 82
        Me.Label5.Text = "Auth Name"
        '
        'TextBoxDisplayName
        '
        Me.TextBoxDisplayName.Location = New System.Drawing.Point(83, 32)
        Me.TextBoxDisplayName.Name = "TextBoxDisplayName"
        Me.TextBoxDisplayName.Size = New System.Drawing.Size(132, 20)
        Me.TextBoxDisplayName.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 36)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 13)
        Me.Label6.TabIndex = 81
        Me.Label6.Text = "Dispaly Name"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(609, 217)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(39, 13)
        Me.Label23.TabIndex = 84
        Me.Label23.Text = "SRTP:"
        '
        'ComboBoxTransport
        '
        Me.ComboBoxTransport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxTransport.FormattingEnabled = True
        Me.ComboBoxTransport.Location = New System.Drawing.Point(528, 215)
        Me.ComboBoxTransport.Name = "ComboBoxTransport"
        Me.ComboBoxTransport.Size = New System.Drawing.Size(64, 21)
        Me.ComboBoxTransport.TabIndex = 51
        '
        'ComboBoxSRTP
        '
        Me.ComboBoxSRTP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxSRTP.FormattingEnabled = True
        Me.ComboBoxSRTP.Location = New System.Drawing.Point(650, 214)
        Me.ComboBoxSRTP.Name = "ComboBoxSRTP"
        Me.ComboBoxSRTP.Size = New System.Drawing.Size(63, 21)
        Me.ComboBoxSRTP.TabIndex = 51
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(471, 218)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(55, 13)
        Me.Label22.TabIndex = 84
        Me.Label22.Text = "Transport:"
        '
        'CheckBoxG7221
        '
        Me.CheckBoxG7221.AutoSize = True
        Me.CheckBoxG7221.Checked = True
        Me.CheckBoxG7221.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxG7221.Location = New System.Drawing.Point(772, 118)
        Me.CheckBoxG7221.Name = "CheckBoxG7221"
        Me.CheckBoxG7221.Size = New System.Drawing.Size(61, 17)
        Me.CheckBoxG7221.TabIndex = 59
        Me.CheckBoxG7221.Text = "G722.1"
        Me.CheckBoxG7221.UseVisualStyleBackColor = True
        '
        'CheckBoxSpeexWB
        '
        Me.CheckBoxSpeexWB.AutoSize = True
        Me.CheckBoxSpeexWB.Checked = True
        Me.CheckBoxSpeexWB.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxSpeexWB.Location = New System.Drawing.Point(627, 118)
        Me.CheckBoxSpeexWB.Name = "CheckBoxSpeexWB"
        Me.CheckBoxSpeexWB.Size = New System.Drawing.Size(71, 17)
        Me.CheckBoxSpeexWB.TabIndex = 58
        Me.CheckBoxSpeexWB.Text = "speex-wb"
        Me.CheckBoxSpeexWB.UseVisualStyleBackColor = True
        '
        'CheckBoxAMRwb
        '
        Me.CheckBoxAMRwb.AutoSize = True
        Me.CheckBoxAMRwb.Checked = True
        Me.CheckBoxAMRwb.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxAMRwb.Location = New System.Drawing.Point(472, 118)
        Me.CheckBoxAMRwb.Name = "CheckBoxAMRwb"
        Me.CheckBoxAMRwb.Size = New System.Drawing.Size(67, 17)
        Me.CheckBoxAMRwb.TabIndex = 57
        Me.CheckBoxAMRwb.Text = "AMR-wb"
        Me.CheckBoxAMRwb.UseVisualStyleBackColor = True
        '
        'CheckBoxSpeex
        '
        Me.CheckBoxSpeex.AutoSize = True
        Me.CheckBoxSpeex.Checked = True
        Me.CheckBoxSpeex.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxSpeex.Location = New System.Drawing.Point(627, 90)
        Me.CheckBoxSpeex.Name = "CheckBoxSpeex"
        Me.CheckBoxSpeex.Size = New System.Drawing.Size(54, 17)
        Me.CheckBoxSpeex.TabIndex = 58
        Me.CheckBoxSpeex.Text = "speex"
        Me.CheckBoxSpeex.UseVisualStyleBackColor = True
        '
        'CheckBoxG722
        '
        Me.CheckBoxG722.AutoSize = True
        Me.CheckBoxG722.Checked = True
        Me.CheckBoxG722.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxG722.Location = New System.Drawing.Point(471, 87)
        Me.CheckBoxG722.Name = "CheckBoxG722"
        Me.CheckBoxG722.Size = New System.Drawing.Size(52, 17)
        Me.CheckBoxG722.TabIndex = 57
        Me.CheckBoxG722.Text = "G722"
        Me.CheckBoxG722.UseVisualStyleBackColor = True
        '
        'CheckBoxILBC
        '
        Me.CheckBoxILBC.AutoSize = True
        Me.CheckBoxILBC.Checked = True
        Me.CheckBoxILBC.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxILBC.Location = New System.Drawing.Point(472, 64)
        Me.CheckBoxILBC.Name = "CheckBoxILBC"
        Me.CheckBoxILBC.Size = New System.Drawing.Size(48, 17)
        Me.CheckBoxILBC.TabIndex = 57
        Me.CheckBoxILBC.Text = "iLBC"
        Me.CheckBoxILBC.UseVisualStyleBackColor = True
        '
        'CheckBoxG729
        '
        Me.CheckBoxG729.AutoSize = True
        Me.CheckBoxG729.Checked = True
        Me.CheckBoxG729.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxG729.Location = New System.Drawing.Point(772, 38)
        Me.CheckBoxG729.Name = "CheckBoxG729"
        Me.CheckBoxG729.Size = New System.Drawing.Size(52, 17)
        Me.CheckBoxG729.TabIndex = 56
        Me.CheckBoxG729.Text = "G729"
        Me.CheckBoxG729.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(773, 304)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(60, 23)
        Me.Button16.TabIndex = 50
        Me.Button16.Text = "UnHold"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'CheckBoxPCMA
        '
        Me.CheckBoxPCMA.AutoSize = True
        Me.CheckBoxPCMA.Checked = True
        Me.CheckBoxPCMA.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxPCMA.Location = New System.Drawing.Point(627, 38)
        Me.CheckBoxPCMA.Name = "CheckBoxPCMA"
        Me.CheckBoxPCMA.Size = New System.Drawing.Size(81, 17)
        Me.CheckBoxPCMA.TabIndex = 55
        Me.CheckBoxPCMA.Text = "G711 aLaw"
        Me.CheckBoxPCMA.UseVisualStyleBackColor = True
        '
        'CheckBoxPCMU
        '
        Me.CheckBoxPCMU.AutoSize = True
        Me.CheckBoxPCMU.Checked = True
        Me.CheckBoxPCMU.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxPCMU.Location = New System.Drawing.Point(472, 38)
        Me.CheckBoxPCMU.Name = "CheckBoxPCMU"
        Me.CheckBoxPCMU.Size = New System.Drawing.Size(81, 17)
        Me.CheckBoxPCMU.TabIndex = 54
        Me.CheckBoxPCMU.Text = "G711 uLaw"
        Me.CheckBoxPCMU.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Location = New System.Drawing.Point(472, 30)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(400, 2)
        Me.GroupBox6.TabIndex = 53
        Me.GroupBox6.TabStop = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Location = New System.Drawing.Point(459, 9)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(2, 555)
        Me.GroupBox5.TabIndex = 51
        Me.GroupBox5.TabStop = False
        '
        'TrackBarVideoQuality
        '
        Me.TrackBarVideoQuality.Location = New System.Drawing.Point(244, 192)
        Me.TrackBarVideoQuality.Maximum = 6
        Me.TrackBarVideoQuality.Minimum = 1
        Me.TrackBarVideoQuality.Name = "TrackBarVideoQuality"
        Me.TrackBarVideoQuality.Size = New System.Drawing.Size(78, 45)
        Me.TrackBarVideoQuality.TabIndex = 3
        Me.TrackBarVideoQuality.TickStyle = System.Windows.Forms.TickStyle.None
        Me.TrackBarVideoQuality.Value = 5
        '
        'ComboBoxVideoResolution
        '
        Me.ComboBoxVideoResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxVideoResolution.FormattingEnabled = True
        Me.ComboBoxVideoResolution.Location = New System.Drawing.Point(641, 278)
        Me.ComboBoxVideoResolution.Name = "ComboBoxVideoResolution"
        Me.ComboBoxVideoResolution.Size = New System.Drawing.Size(90, 21)
        Me.ComboBoxVideoResolution.TabIndex = 52
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(583, 282)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(57, 13)
        Me.Label18.TabIndex = 51
        Me.Label18.Text = "Resolution"
        '
        'ListBoxSIPLog
        '
        Me.ListBoxSIPLog.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBoxSIPLog.ForeColor = System.Drawing.Color.Black
        Me.ListBoxSIPLog.FormattingEnabled = True
        Me.ListBoxSIPLog.ItemHeight = 12
        Me.ListBoxSIPLog.Location = New System.Drawing.Point(1, 70)
        Me.ListBoxSIPLog.Name = "ListBoxSIPLog"
        Me.ListBoxSIPLog.Size = New System.Drawing.Size(246, 232)
        Me.ListBoxSIPLog.TabIndex = 40
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.GroupBox16)
        Me.GroupBox3.Controls.Add(Me.Button3)
        Me.GroupBox3.Controls.Add(Me.Button12)
        Me.GroupBox3.Controls.Add(Me.Button13)
        Me.GroupBox3.Controls.Add(Me.Button14)
        Me.GroupBox3.Controls.Add(Me.Button9)
        Me.GroupBox3.Controls.Add(Me.Button10)
        Me.GroupBox3.Controls.Add(Me.Button11)
        Me.GroupBox3.Controls.Add(Me.Button6)
        Me.GroupBox3.Controls.Add(Me.Button7)
        Me.GroupBox3.Controls.Add(Me.Button8)
        Me.GroupBox3.Controls.Add(Me.Button5)
        Me.GroupBox3.Controls.Add(Me.Button4)
        Me.GroupBox3.Location = New System.Drawing.Point(265, 24)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(257, 46)
        Me.GroupBox3.TabIndex = 84
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Dial Pad"
        '
        'GroupBox16
        '
        Me.GroupBox16.Controls.Add(Me.Label24)
        Me.GroupBox16.Controls.Add(Me.TextBoxPhoneNumber)
        Me.GroupBox16.Controls.Add(Me.ButtonDial)
        Me.GroupBox16.Controls.Add(Me.ButtonHangUp)
        Me.GroupBox16.Enabled = False
        Me.GroupBox16.Location = New System.Drawing.Point(9, 8)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(248, 32)
        Me.GroupBox16.TabIndex = 88
        Me.GroupBox16.TabStop = False
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(4, 15)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(44, 13)
        Me.Label24.TabIndex = 32
        Me.Label24.Text = "Number"
        '
        'TrackBarSpeaker
        '
        Me.TrackBarSpeaker.Location = New System.Drawing.Point(242, 112)
        Me.TrackBarSpeaker.Maximum = 65535
        Me.TrackBarSpeaker.Name = "TrackBarSpeaker"
        Me.TrackBarSpeaker.Size = New System.Drawing.Size(80, 45)
        Me.TrackBarSpeaker.TabIndex = 1
        Me.TrackBarSpeaker.TickFrequency = 1000
        Me.TrackBarSpeaker.TickStyle = System.Windows.Forms.TickStyle.None
        '
        'ButtonTestAudio
        '
        Me.ButtonTestAudio.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonTestAudio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonTestAudio.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonTestAudio.ForeColor = System.Drawing.Color.Black
        Me.ButtonTestAudio.Location = New System.Drawing.Point(248, 165)
        Me.ButtonTestAudio.Name = "ButtonTestAudio"
        Me.ButtonTestAudio.Size = New System.Drawing.Size(68, 22)
        Me.ButtonTestAudio.TabIndex = 41
        Me.ButtonTestAudio.Text = "Test Audio"
        Me.ButtonTestAudio.UseVisualStyleBackColor = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(6, 115)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(47, 14)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Speaker"
        '
        'TrackBarMicrophone
        '
        Me.TrackBarMicrophone.Location = New System.Drawing.Point(242, 141)
        Me.TrackBarMicrophone.Maximum = 65535
        Me.TrackBarMicrophone.Name = "TrackBarMicrophone"
        Me.TrackBarMicrophone.Size = New System.Drawing.Size(80, 45)
        Me.TrackBarMicrophone.TabIndex = 3
        Me.TrackBarMicrophone.TickFrequency = 1000
        Me.TrackBarMicrophone.TickStyle = System.Windows.Forms.TickStyle.None
        '
        'CheckBoxMute
        '
        Me.CheckBoxMute.AutoSize = True
        Me.CheckBoxMute.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxMute.ForeColor = System.Drawing.Color.Black
        Me.CheckBoxMute.Location = New System.Drawing.Point(71, 167)
        Me.CheckBoxMute.Name = "CheckBoxMute"
        Me.CheckBoxMute.Size = New System.Drawing.Size(108, 18)
        Me.CheckBoxMute.TabIndex = 45
        Me.CheckBoxMute.Text = "Mute microphone"
        Me.CheckBoxMute.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(6, 144)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(63, 14)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "Microphone"
        '
        'GroupBox12
        '
        Me.GroupBox12.BackColor = System.Drawing.Color.White
        Me.GroupBox12.Controls.Add(Me.ButtonTestVideo)
        Me.GroupBox12.Controls.Add(Me.CheckBox1)
        Me.GroupBox12.Controls.Add(Me.ButtonTestAudio)
        Me.GroupBox12.Controls.Add(Me.TrackBarMicrophone)
        Me.GroupBox12.Controls.Add(Me.ComboBoxMicrophones)
        Me.GroupBox12.Controls.Add(Me.CheckBoxMute)
        Me.GroupBox12.Controls.Add(Me.TrackBarSpeaker)
        Me.GroupBox12.Controls.Add(Me.ComboBoxSpeakers)
        Me.GroupBox12.Controls.Add(Me.ComboBoxCameras)
        Me.GroupBox12.Controls.Add(Me.Label14)
        Me.GroupBox12.Controls.Add(Me.GroupBox15)
        Me.GroupBox12.Controls.Add(Me.Label11)
        Me.GroupBox12.Controls.Add(Me.Label10)
        Me.GroupBox12.Controls.Add(Me.TrackBarVideoQuality)
        Me.GroupBox12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox12.Location = New System.Drawing.Point(253, 65)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(324, 239)
        Me.GroupBox12.TabIndex = 34
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Phone Settings"
        '
        'ButtonTestVideo
        '
        Me.ButtonTestVideo.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonTestVideo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonTestVideo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonTestVideo.ForeColor = System.Drawing.Color.Black
        Me.ButtonTestVideo.Location = New System.Drawing.Point(248, 214)
        Me.ButtonTestVideo.Name = "ButtonTestVideo"
        Me.ButtonTestVideo.Size = New System.Drawing.Size(68, 22)
        Me.ButtonTestVideo.TabIndex = 88
        Me.ButtonTestVideo.Text = "Test Video"
        Me.ButtonTestVideo.UseVisualStyleBackColor = False
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.Black
        Me.CheckBox1.Location = New System.Drawing.Point(70, 217)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(87, 18)
        Me.CheckBox1.TabIndex = 87
        Me.CheckBox1.Text = "Pause Video"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'GroupBox15
        '
        Me.GroupBox15.Controls.Add(Me.TextBoxUserName)
        Me.GroupBox15.Controls.Add(Me.Label3)
        Me.GroupBox15.Controls.Add(Me.TextBoxServerPort)
        Me.GroupBox15.Controls.Add(Me.Label4)
        Me.GroupBox15.Controls.Add(Me.TextBoxServer)
        Me.GroupBox15.Controls.Add(Me.Label1)
        Me.GroupBox15.Controls.Add(Me.Button2)
        Me.GroupBox15.Controls.Add(Me.Button1)
        Me.GroupBox15.Location = New System.Drawing.Point(7, 12)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Size = New System.Drawing.Size(308, 95)
        Me.GroupBox15.TabIndex = 86
        Me.GroupBox15.TabStop = False
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(16, 26)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(51, 13)
        Me.Label27.TabIndex = 8
        Me.Label27.Text = "select file"
        '
        'TextBoxPlayFile
        '
        Me.TextBoxPlayFile.Location = New System.Drawing.Point(16, 56)
        Me.TextBoxPlayFile.Name = "TextBoxPlayFile"
        Me.TextBoxPlayFile.ReadOnly = True
        Me.TextBoxPlayFile.Size = New System.Drawing.Size(183, 20)
        Me.TextBoxPlayFile.TabIndex = 9
        '
        'Button20
        '
        Me.Button20.Location = New System.Drawing.Point(78, 22)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(69, 23)
        Me.Button20.TabIndex = 10
        Me.Button20.Text = "..."
        Me.Button20.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.Button23)
        Me.GroupBox8.Controls.Add(Me.Button21)
        Me.GroupBox8.Controls.Add(Me.Button20)
        Me.GroupBox8.Controls.Add(Me.TextBoxPlayFile)
        Me.GroupBox8.Controls.Add(Me.Label27)
        Me.GroupBox8.Location = New System.Drawing.Point(802, 615)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(212, 124)
        Me.GroupBox8.TabIndex = 26
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Play audio file(Wave)"
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.Button28)
        Me.GroupBox11.Controls.Add(Me.Button27)
        Me.GroupBox11.Controls.Add(Me.Label17)
        Me.GroupBox11.Controls.Add(Me.CheckBoxForwardCallBusy)
        Me.GroupBox11.Controls.Add(Me.TextBoxForwardTo)
        Me.GroupBox11.Location = New System.Drawing.Point(1020, 615)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(200, 124)
        Me.GroupBox11.TabIndex = 29
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Call Forward"
        '
        'Button28
        '
        Me.Button28.Location = New System.Drawing.Point(109, 91)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(75, 23)
        Me.Button28.TabIndex = 5
        Me.Button28.Text = "Disable"
        Me.Button28.UseVisualStyleBackColor = True
        '
        'Button27
        '
        Me.Button27.Location = New System.Drawing.Point(12, 91)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(75, 23)
        Me.Button27.TabIndex = 4
        Me.Button27.Text = "Enable"
        Me.Button27.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(9, 22)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(79, 13)
        Me.Label17.TabIndex = 3
        Me.Label17.Text = "Forward call to:"
        '
        'CheckBoxForwardCallBusy
        '
        Me.CheckBoxForwardCallBusy.AutoSize = True
        Me.CheckBoxForwardCallBusy.Location = New System.Drawing.Point(12, 70)
        Me.CheckBoxForwardCallBusy.Name = "CheckBoxForwardCallBusy"
        Me.CheckBoxForwardCallBusy.Size = New System.Drawing.Size(160, 17)
        Me.CheckBoxForwardCallBusy.TabIndex = 2
        Me.CheckBoxForwardCallBusy.Text = "Forward call when on phone"
        Me.CheckBoxForwardCallBusy.UseVisualStyleBackColor = True
        '
        'TextBoxForwardTo
        '
        Me.TextBoxForwardTo.Location = New System.Drawing.Point(12, 41)
        Me.TextBoxForwardTo.Name = "TextBoxForwardTo"
        Me.TextBoxForwardTo.Size = New System.Drawing.Size(172, 20)
        Me.TextBoxForwardTo.TabIndex = 1
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(1188, 47)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(172, 13)
        Me.LinkLabel1.TabIndex = 32
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Click here to visit PortSIP's website"
        Me.LinkLabel1.Visible = False
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.Location = New System.Drawing.Point(1188, 80)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(170, 13)
        Me.LinkLabel2.TabIndex = 33
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Click here to send email to PortSIP"
        Me.LinkLabel2.Visible = False
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.CheckBoxAA)
        Me.GroupBox14.Controls.Add(Me.CheckBoxSDP)
        Me.GroupBox14.Controls.Add(Me.CheckBoxDND)
        Me.GroupBox14.Controls.Add(Me.CheckBoxConf)
        Me.GroupBox14.Location = New System.Drawing.Point(2, 485)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(297, 43)
        Me.GroupBox14.TabIndex = 85
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "Speaker"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SettingsToolStripMenuItem, Me.ExitToolStripMenuItem, Me.ShowToolStripMenuItem1, Me.AboutToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(577, 24)
        Me.MenuStrip1.TabIndex = 42
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ShowToolStripMenuItem, Me.CloseToolStripMenuItem})
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.SettingsToolStripMenuItem.Text = "&Settings"
        '
        'ShowToolStripMenuItem
        '
        Me.ShowToolStripMenuItem.Name = "ShowToolStripMenuItem"
        Me.ShowToolStripMenuItem.Size = New System.Drawing.Size(103, 22)
        Me.ShowToolStripMenuItem.Text = "Show"
        '
        'CloseToolStripMenuItem
        '
        Me.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem"
        Me.CloseToolStripMenuItem.Size = New System.Drawing.Size(103, 22)
        Me.CloseToolStripMenuItem.Text = "Hide"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.ExitToolStripMenuItem.Text = "&Exit"
        Me.ExitToolStripMenuItem.Visible = False
        '
        'ShowToolStripMenuItem1
        '
        Me.ShowToolStripMenuItem1.Name = "ShowToolStripMenuItem1"
        Me.ShowToolStripMenuItem1.Size = New System.Drawing.Size(48, 20)
        Me.ShowToolStripMenuItem1.Text = "Show"
        Me.ShowToolStripMenuItem1.Visible = False
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(0, 24)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(132, 40)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 43
        Me.PictureBox2.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(526, 24)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(49, 40)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 86
        Me.PictureBox3.TabStop = False
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Visible = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.KeyPadToolStripMenuItem1, Me.AboutToolStripMenuItem1, Me.ToolStripMenuItem2})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(117, 92)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(116, 22)
        Me.ToolStripMenuItem1.Text = "Show"
        '
        'KeyPadToolStripMenuItem1
        '
        Me.KeyPadToolStripMenuItem1.Name = "KeyPadToolStripMenuItem1"
        Me.KeyPadToolStripMenuItem1.Size = New System.Drawing.Size(116, 22)
        Me.KeyPadToolStripMenuItem1.Text = "Key Pad"
        '
        'AboutToolStripMenuItem1
        '
        Me.AboutToolStripMenuItem1.Name = "AboutToolStripMenuItem1"
        Me.AboutToolStripMenuItem1.Size = New System.Drawing.Size(116, 22)
        Me.AboutToolStripMenuItem1.Text = "About"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(116, 22)
        Me.ToolStripMenuItem2.Text = "Exit"
        '
        'Timer1
        '
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 24)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(852, 40)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 41
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'Subscribe
        '
        '
        'Timer2
        '
        Me.Timer2.Enabled = True
        Me.Timer2.Interval = 1000
        '
        'SmallVideo
        '
        Me.SmallVideo.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SmallVideo.BackgroundImage = CType(resources.GetObject("SmallVideo.BackgroundImage"), System.Drawing.Image)
        Me.SmallVideo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SmallVideo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SmallVideo.ErrorImage = Nothing
        Me.SmallVideo.InitialImage = Nothing
        Me.SmallVideo.Location = New System.Drawing.Point(755, 219)
        Me.SmallVideo.Name = "SmallVideo"
        Me.SmallVideo.Size = New System.Drawing.Size(97, 82)
        Me.SmallVideo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SmallVideo.TabIndex = 89
        Me.SmallVideo.TabStop = False
        '
        'BigVideo
        '
        Me.BigVideo.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BigVideo.BackgroundImage = CType(resources.GetObject("BigVideo.BackgroundImage"), System.Drawing.Image)
        Me.BigVideo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BigVideo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.BigVideo.ErrorImage = Nothing
        Me.BigVideo.InitialImage = Nothing
        Me.BigVideo.Location = New System.Drawing.Point(583, 70)
        Me.BigVideo.Name = "BigVideo"
        Me.BigVideo.Size = New System.Drawing.Size(269, 234)
        Me.BigVideo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.BigVideo.TabIndex = 88
        Me.BigVideo.TabStop = False
        '
        'AxWindowsMediaPlayer1
        '
        Me.AxWindowsMediaPlayer1.Enabled = True
        Me.AxWindowsMediaPlayer1.Location = New System.Drawing.Point(0, 394)
        Me.AxWindowsMediaPlayer1.Name = "AxWindowsMediaPlayer1"
        Me.AxWindowsMediaPlayer1.OcxState = CType(resources.GetObject("AxWindowsMediaPlayer1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxWindowsMediaPlayer1.Size = New System.Drawing.Size(75, 23)
        Me.AxWindowsMediaPlayer1.TabIndex = 87
        '
        'AxPortSIPCoreLib1
        '
        Me.AxPortSIPCoreLib1.Enabled = True
        Me.AxPortSIPCoreLib1.Location = New System.Drawing.Point(66, 616)
        Me.AxPortSIPCoreLib1.Name = "AxPortSIPCoreLib1"
        Me.AxPortSIPCoreLib1.OcxState = CType(resources.GetObject("AxPortSIPCoreLib1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxPortSIPCoreLib1.Size = New System.Drawing.Size(100, 50)
        Me.AxPortSIPCoreLib1.TabIndex = 30
        Me.AxPortSIPCoreLib1.Visible = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(577, 307)
        Me.Controls.Add(Me.SmallVideo)
        Me.Controls.Add(Me.BigVideo)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.ComboBoxVideoResolution)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.AxWindowsMediaPlayer1)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.CheckBoxAGC)
        Me.Controls.Add(Me.CheckBoxCNG)
        Me.Controls.Add(Me.CheckBoxVAD)
        Me.Controls.Add(Me.CheckBoxAEC)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.GroupBox14)
        Me.Controls.Add(Me.ListBoxSIPLog)
        Me.Controls.Add(Me.LinkLabel2)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.AxPortSIPCoreLib1)
        Me.Controls.Add(Me.GroupBox11)
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.GroupBox12)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(869, 346)
        Me.MinimumSize = New System.Drawing.Size(593, 346)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CSI SIP Phone - "
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.AxDeviceManagerLib1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBarVideoQuality, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox16.PerformLayout()
        CType(Me.TrackBarSpeaker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBarMicrophone, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox15.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SmallVideo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BigVideo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxWindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxPortSIPCoreLib1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CheckBoxConf As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxAA As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxDND As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button22 As System.Windows.Forms.Button
    Friend WithEvents CheckBoxSDP As System.Windows.Forms.CheckBox
    Friend WithEvents TextBoxRecordFilePath As System.Windows.Forms.TextBox
    Friend WithEvents ButtonAnswer As System.Windows.Forms.Button
    Friend WithEvents ButtonTransfer As System.Windows.Forms.Button
    Friend WithEvents ButtonHold As System.Windows.Forms.Button
    Friend WithEvents ComboBoxLines As System.Windows.Forms.ComboBox
    Friend WithEvents ButtonReject As System.Windows.Forms.Button
    Friend WithEvents ButtonHangUp As System.Windows.Forms.Button
    Friend WithEvents ButtonDial As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TextBoxPhoneNumber As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBoxStunPort As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBoxStunServer As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBoxServerPort As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBoxServer As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBoxPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBoxUserName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents ButtonSendVideo As System.Windows.Forms.Button
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents CheckBoxH2631998 As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonLocalVideo As System.Windows.Forms.Button
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents TextBoxAudioRecordFileName As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents ButtonCameraOptions As System.Windows.Forms.Button
    Friend WithEvents Button24 As System.Windows.Forms.Button
    Friend WithEvents ButtonVideoPreview As System.Windows.Forms.Button
    Friend WithEvents ComboBoxMicrophones As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxAGC As System.Windows.Forms.CheckBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxCNG As System.Windows.Forms.CheckBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxVAD As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxAEC As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBoxSpeakers As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBoxCameras As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBoxH264 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxG723 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxGSM As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxH263 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxILBC As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxG729 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPCMA As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPCMU As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents TextBoxPlayFile As System.Windows.Forms.TextBox
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextBoxVideoRecordFileName As System.Windows.Forms.TextBox
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents CheckBoxAudioStream As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxForwardCallBusy As System.Windows.Forms.CheckBox
    Friend WithEvents TextBoxForwardTo As System.Windows.Forms.TextBox
    Friend WithEvents Button28 As System.Windows.Forms.Button
    Friend WithEvents Button27 As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents AxPortSIPCoreLib1 As AxPortSIPCoreLibLib.AxPortSIPCoreLib
    Friend WithEvents AxDeviceManagerLib1 As AxDeviceManagerLibLib.AxDeviceManagerLib
    Friend WithEvents TextBoxUserDomain As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBoxAuthName As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBoxDisplayName As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
    Friend WithEvents ComboBoxTransport As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxSRTP As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBoxTimestamp As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxG7221 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxSpeexWB As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxAMRwb As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxSpeex As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxG722 As System.Windows.Forms.CheckBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents TrackBarVideoQuality As System.Windows.Forms.TrackBar
    Friend WithEvents ComboBoxVideoResolution As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TrackBarMicrophone As System.Windows.Forms.TrackBar
    Friend WithEvents CheckBoxMute As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonTestAudio As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TrackBarSpeaker As System.Windows.Forms.TrackBar
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ListBoxSIPLog As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox15 As System.Windows.Forms.GroupBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShowToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CloseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents ShowToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents KeyPadToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Subscribe As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents AxWindowsMediaPlayer1 As AxWMPLib.AxWindowsMediaPlayer
    Friend WithEvents ButtonTestVideo As Button
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents SmallVideo As PictureBox
    Friend WithEvents BigVideo As PictureBox
End Class
