﻿Public Class frmEmail2Case
    Dim cntEmailtoCase As Integer = 0

    Private Sub frmEmail2Case_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            wbEmailToCase.Visible = False
            wbEmailToCase.Navigate(My.Settings.Email2CaseURL)
            'wbEmailToCase.Visible = True
            cntEmailtoCase = 0
            tmrEmail2Case.Interval = 1000
            tmrEmail2Case.Enabled = True
        Catch ex As Exception
            'Do Nothing
        End Try
    End Sub

    Private Sub tmrEmail2Case_Tick(sender As Object, e As EventArgs) Handles tmrEmail2Case.Tick
        Try
            cntEmailtoCase += 1
            If (cntEmailtoCase > Integer.Parse(My.Settings.Email2CaseTimeDur)) Then
                tmrEmail2Case.Enabled = False
                Me.Close()
            End If
        Catch ex As Exception
            'Do Nothing
        End Try
    End Sub

    Private Sub frmEmail2Case_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        wbEmailToCase.Top = 0
        wbEmailToCase.Left = 0
        wbEmailToCase.Height = Me.Height
        wbEmailToCase.Width = Me.Width
    End Sub

    Private Sub wbEmailToCase_Navigated(sender As Object, e As WebBrowserNavigatedEventArgs) Handles wbEmailToCase.Navigated
        wbEmailToCase.Visible = True
    End Sub
End Class
