Public Class frmKeypad

    Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn1.Click
        Form1.sendDTMF(1)
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        Form1.sendDTMF(2)
    End Sub

    Private Sub btn3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn3.Click
        Form1.sendDTMF(3)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        Form1.sendDTMF(4)
    End Sub

    Private Sub btn5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn5.Click
        Form1.sendDTMF(5)
    End Sub

    Private Sub btn6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn6.Click
        Form1.sendDTMF(6)
    End Sub

    Private Sub btn7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn7.Click
        Form1.sendDTMF(7)
    End Sub

    Private Sub btn8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn8.Click
        Form1.sendDTMF(8)
    End Sub

    Private Sub btn9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn9.Click
        Form1.sendDTMF(9)
    End Sub

    Private Sub btnStar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStar.Click
        Form1.sendDTMF(10)
    End Sub

    Private Sub btn0_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn0.Click
        Form1.sendDTMF(0)
    End Sub

    Private Sub btnHash_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHash.Click
        Form1.sendDTMF(11)
    End Sub

    Private Sub lblCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblCLose.Click
        Me.Hide()
    End Sub

    Private Sub frmKeypad_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.TopMost = True
    End Sub
End Class