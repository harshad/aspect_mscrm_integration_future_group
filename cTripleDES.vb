Imports System.IO
Imports System.Text
Imports System.Security.Cryptography

Public Class cTripleDES

    'Found From : - http://www.codeproject.com/KB/vb/VB_NET_TripleDES.aspx

    'Another Referrence From : - http://www.example-code.com/vbdotnet/Pki3Des.asp

    ' define the triple des provider

    Private m_des As New TripleDESCryptoServiceProvider

    ' define the string handler

    Private m_utf8 As New UTF8Encoding

    ' define the local property arrays

    ReadOnly m_key() As Byte = _
          {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, _
          15, 16, 17, 18, 19, 20, 21, 22, 23, 24}
    ReadOnly m_iv() As Byte = {8, 7, 6, 5, 4, 3, 2, 1}

    Public Sub New()

    End Sub

    Public Function Encrypt(ByVal input() As Byte) As Byte()
        Return Transform(input, m_des.CreateEncryptor(m_key, m_iv))
    End Function

    Public Function Decrypt(ByVal input() As Byte) As Byte()
        Return Transform(input, m_des.CreateDecryptor(m_key, m_iv))
    End Function

    Public Function Encrypt(ByVal text As String) As String
        If Trim(text) = "" Then
            Return ""
        Else
            Dim input() As Byte = m_utf8.GetBytes(text)
            Dim output() As Byte = Transform(input, _
                            m_des.CreateEncryptor(m_key, m_iv))
            Return Convert.ToBase64String(output) & "====="
        End If
    End Function

    Public Function Decrypt(ByVal text As String) As String
        Try
            If Trim(text) = "" Then
                Return ""
            Else
                Dim input() As Byte = Convert.FromBase64String(Left(text, Len(text) - 5))
                Dim output() As Byte = Transform(input, _
                                 m_des.CreateDecryptor(m_key, m_iv))
                Return m_utf8.GetString(output)
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Function Transform(ByVal input() As Byte, _
        ByVal CryptoTransform As ICryptoTransform) As Byte()
        ' create the necessary streams

        Dim memStream As MemoryStream = New MemoryStream
        Dim cryptStream As CryptoStream = New _
            CryptoStream(memStream, CryptoTransform, _
            CryptoStreamMode.Write)
        ' transform the bytes as requested

        cryptStream.Write(input, 0, input.Length)
        cryptStream.FlushFinalBlock()
        ' Read the memory stream and convert it back into byte array

        memStream.Position = 0
        Dim result(CType(memStream.Length - 1, System.Int32)) As Byte
        memStream.Read(result, 0, CType(result.Length, System.Int32))
        ' close and release the streams

        memStream.Close()
        cryptStream.Close()
        ' hand back the encrypted buffer

        Return result
    End Function

End Class
