﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

'
'This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
'
Namespace MOHRE_WS
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Web.Services.WebServiceBindingAttribute(Name:="ServiceSoap", [Namespace]:="http://tempuri.org/")>  _
    Partial Public Class Service
        Inherits System.Web.Services.Protocols.SoapHttpClientProtocol
        
        Private ResetAgentAudioExtOperationCompleted As System.Threading.SendOrPostCallback
        
        Private GetAgentVideoExtOperationCompleted As System.Threading.SendOrPostCallback
        
        Private mapVoiceExtnOperationCompleted As System.Threading.SendOrPostCallback
        
        Private setLanguagePreferenceOperationCompleted As System.Threading.SendOrPostCallback
        
        Private getLanguagePreferenceOperationCompleted As System.Threading.SendOrPostCallback
        
        Private useDefaultCredentialsSetExplicitly As Boolean
        
        '''<remarks/>
        Public Sub New()
            MyBase.New
            Me.Url = Global.CSISIP.My.MySettings.Default.CSISIP_MOHRE_WS_Service
            If (Me.IsLocalFileSystemWebService(Me.Url) = true) Then
                Me.UseDefaultCredentials = true
                Me.useDefaultCredentialsSetExplicitly = false
            Else
                Me.useDefaultCredentialsSetExplicitly = true
            End If
        End Sub
        
        Public Shadows Property Url() As String
            Get
                Return MyBase.Url
            End Get
            Set
                If (((Me.IsLocalFileSystemWebService(MyBase.Url) = true)  _
                            AndAlso (Me.useDefaultCredentialsSetExplicitly = false))  _
                            AndAlso (Me.IsLocalFileSystemWebService(value) = false)) Then
                    MyBase.UseDefaultCredentials = false
                End If
                MyBase.Url = value
            End Set
        End Property
        
        Public Shadows Property UseDefaultCredentials() As Boolean
            Get
                Return MyBase.UseDefaultCredentials
            End Get
            Set
                MyBase.UseDefaultCredentials = value
                Me.useDefaultCredentialsSetExplicitly = true
            End Set
        End Property
        
        '''<remarks/>
        Public Event ResetAgentAudioExtCompleted As ResetAgentAudioExtCompletedEventHandler
        
        '''<remarks/>
        Public Event GetAgentVideoExtCompleted As GetAgentVideoExtCompletedEventHandler
        
        '''<remarks/>
        Public Event mapVoiceExtnCompleted As mapVoiceExtnCompletedEventHandler
        
        '''<remarks/>
        Public Event setLanguagePreferenceCompleted As setLanguagePreferenceCompletedEventHandler
        
        '''<remarks/>
        Public Event getLanguagePreferenceCompleted As getLanguagePreferenceCompletedEventHandler
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/ResetAgentAudioExt", RequestNamespace:="http://tempuri.org/", ResponseNamespace:="http://tempuri.org/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function ResetAgentAudioExt(ByVal agentAudioExt As Integer) As Boolean
            Dim results() As Object = Me.Invoke("ResetAgentAudioExt", New Object() {agentAudioExt})
            Return CType(results(0),Boolean)
        End Function
        
        '''<remarks/>
        Public Overloads Sub ResetAgentAudioExtAsync(ByVal agentAudioExt As Integer)
            Me.ResetAgentAudioExtAsync(agentAudioExt, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub ResetAgentAudioExtAsync(ByVal agentAudioExt As Integer, ByVal userState As Object)
            If (Me.ResetAgentAudioExtOperationCompleted Is Nothing) Then
                Me.ResetAgentAudioExtOperationCompleted = AddressOf Me.OnResetAgentAudioExtOperationCompleted
            End If
            Me.InvokeAsync("ResetAgentAudioExt", New Object() {agentAudioExt}, Me.ResetAgentAudioExtOperationCompleted, userState)
        End Sub
        
        Private Sub OnResetAgentAudioExtOperationCompleted(ByVal arg As Object)
            If (Not (Me.ResetAgentAudioExtCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent ResetAgentAudioExtCompleted(Me, New ResetAgentAudioExtCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetAgentVideoExt", RequestNamespace:="http://tempuri.org/", ResponseNamespace:="http://tempuri.org/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function GetAgentVideoExt(ByVal KioskVideoExt As Integer) As GetAgentVideoExt_Resp
            Dim results() As Object = Me.Invoke("GetAgentVideoExt", New Object() {KioskVideoExt})
            Return CType(results(0),GetAgentVideoExt_Resp)
        End Function
        
        '''<remarks/>
        Public Overloads Sub GetAgentVideoExtAsync(ByVal KioskVideoExt As Integer)
            Me.GetAgentVideoExtAsync(KioskVideoExt, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub GetAgentVideoExtAsync(ByVal KioskVideoExt As Integer, ByVal userState As Object)
            If (Me.GetAgentVideoExtOperationCompleted Is Nothing) Then
                Me.GetAgentVideoExtOperationCompleted = AddressOf Me.OnGetAgentVideoExtOperationCompleted
            End If
            Me.InvokeAsync("GetAgentVideoExt", New Object() {KioskVideoExt}, Me.GetAgentVideoExtOperationCompleted, userState)
        End Sub
        
        Private Sub OnGetAgentVideoExtOperationCompleted(ByVal arg As Object)
            If (Not (Me.GetAgentVideoExtCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent GetAgentVideoExtCompleted(Me, New GetAgentVideoExtCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/mapVoiceExtn", RequestNamespace:="http://tempuri.org/", ResponseNamespace:="http://tempuri.org/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function mapVoiceExtn(ByVal KioskAudioExt As Integer, ByVal AgentAudioExt As Integer) As mapVoiceExtn_Resp
            Dim results() As Object = Me.Invoke("mapVoiceExtn", New Object() {KioskAudioExt, AgentAudioExt})
            Return CType(results(0),mapVoiceExtn_Resp)
        End Function
        
        '''<remarks/>
        Public Overloads Sub mapVoiceExtnAsync(ByVal KioskAudioExt As Integer, ByVal AgentAudioExt As Integer)
            Me.mapVoiceExtnAsync(KioskAudioExt, AgentAudioExt, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub mapVoiceExtnAsync(ByVal KioskAudioExt As Integer, ByVal AgentAudioExt As Integer, ByVal userState As Object)
            If (Me.mapVoiceExtnOperationCompleted Is Nothing) Then
                Me.mapVoiceExtnOperationCompleted = AddressOf Me.OnmapVoiceExtnOperationCompleted
            End If
            Me.InvokeAsync("mapVoiceExtn", New Object() {KioskAudioExt, AgentAudioExt}, Me.mapVoiceExtnOperationCompleted, userState)
        End Sub
        
        Private Sub OnmapVoiceExtnOperationCompleted(ByVal arg As Object)
            If (Not (Me.mapVoiceExtnCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent mapVoiceExtnCompleted(Me, New mapVoiceExtnCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/setLanguagePreference", RequestNamespace:="http://tempuri.org/", ResponseNamespace:="http://tempuri.org/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function setLanguagePreference(ByVal KioskVideoExt As Integer, ByVal LangPref As String) As Integer
            Dim results() As Object = Me.Invoke("setLanguagePreference", New Object() {KioskVideoExt, LangPref})
            Return CType(results(0),Integer)
        End Function
        
        '''<remarks/>
        Public Overloads Sub setLanguagePreferenceAsync(ByVal KioskVideoExt As Integer, ByVal LangPref As String)
            Me.setLanguagePreferenceAsync(KioskVideoExt, LangPref, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub setLanguagePreferenceAsync(ByVal KioskVideoExt As Integer, ByVal LangPref As String, ByVal userState As Object)
            If (Me.setLanguagePreferenceOperationCompleted Is Nothing) Then
                Me.setLanguagePreferenceOperationCompleted = AddressOf Me.OnsetLanguagePreferenceOperationCompleted
            End If
            Me.InvokeAsync("setLanguagePreference", New Object() {KioskVideoExt, LangPref}, Me.setLanguagePreferenceOperationCompleted, userState)
        End Sub
        
        Private Sub OnsetLanguagePreferenceOperationCompleted(ByVal arg As Object)
            If (Not (Me.setLanguagePreferenceCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent setLanguagePreferenceCompleted(Me, New setLanguagePreferenceCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/getLanguagePreference", RequestNamespace:="http://tempuri.org/", ResponseNamespace:="http://tempuri.org/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function getLanguagePreference(ByVal KioskAudioExt As Integer) As String
            Dim results() As Object = Me.Invoke("getLanguagePreference", New Object() {KioskAudioExt})
            Return CType(results(0),String)
        End Function
        
        '''<remarks/>
        Public Overloads Sub getLanguagePreferenceAsync(ByVal KioskAudioExt As Integer)
            Me.getLanguagePreferenceAsync(KioskAudioExt, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub getLanguagePreferenceAsync(ByVal KioskAudioExt As Integer, ByVal userState As Object)
            If (Me.getLanguagePreferenceOperationCompleted Is Nothing) Then
                Me.getLanguagePreferenceOperationCompleted = AddressOf Me.OngetLanguagePreferenceOperationCompleted
            End If
            Me.InvokeAsync("getLanguagePreference", New Object() {KioskAudioExt}, Me.getLanguagePreferenceOperationCompleted, userState)
        End Sub
        
        Private Sub OngetLanguagePreferenceOperationCompleted(ByVal arg As Object)
            If (Not (Me.getLanguagePreferenceCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent getLanguagePreferenceCompleted(Me, New getLanguagePreferenceCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        Public Shadows Sub CancelAsync(ByVal userState As Object)
            MyBase.CancelAsync(userState)
        End Sub
        
        Private Function IsLocalFileSystemWebService(ByVal url As String) As Boolean
            If ((url Is Nothing)  _
                        OrElse (url Is String.Empty)) Then
                Return false
            End If
            Dim wsUri As System.Uri = New System.Uri(url)
            If ((wsUri.Port >= 1024)  _
                        AndAlso (String.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) = 0)) Then
                Return true
            End If
            Return false
        End Function
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class GetAgentVideoExt_Resp
        
        Private statusCodeField As Integer
        
        Private errorDescField As String
        
        Private agentVideoExtField As Integer
        
        '''<remarks/>
        Public Property StatusCode() As Integer
            Get
                Return Me.statusCodeField
            End Get
            Set
                Me.statusCodeField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property ErrorDesc() As String
            Get
                Return Me.errorDescField
            End Get
            Set
                Me.errorDescField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property AgentVideoExt() As Integer
            Get
                Return Me.agentVideoExtField
            End Get
            Set
                Me.agentVideoExtField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class mapVoiceExtn_Resp
        
        Private statusCodeField As Integer
        
        Private errorDescField As String
        
        Private agentVideoExtField As Integer
        
        '''<remarks/>
        Public Property StatusCode() As Integer
            Get
                Return Me.statusCodeField
            End Get
            Set
                Me.statusCodeField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property ErrorDesc() As String
            Get
                Return Me.errorDescField
            End Get
            Set
                Me.errorDescField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property AgentVideoExt() As Integer
            Get
                Return Me.agentVideoExtField
            End Get
            Set
                Me.agentVideoExtField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0")>  _
    Public Delegate Sub ResetAgentAudioExtCompletedEventHandler(ByVal sender As Object, ByVal e As ResetAgentAudioExtCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class ResetAgentAudioExtCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As Boolean
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),Boolean)
            End Get
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0")>  _
    Public Delegate Sub GetAgentVideoExtCompletedEventHandler(ByVal sender As Object, ByVal e As GetAgentVideoExtCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class GetAgentVideoExtCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As GetAgentVideoExt_Resp
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),GetAgentVideoExt_Resp)
            End Get
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0")>  _
    Public Delegate Sub mapVoiceExtnCompletedEventHandler(ByVal sender As Object, ByVal e As mapVoiceExtnCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class mapVoiceExtnCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As mapVoiceExtn_Resp
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),mapVoiceExtn_Resp)
            End Get
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0")>  _
    Public Delegate Sub setLanguagePreferenceCompletedEventHandler(ByVal sender As Object, ByVal e As setLanguagePreferenceCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class setLanguagePreferenceCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As Integer
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),Integer)
            End Get
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0")>  _
    Public Delegate Sub getLanguagePreferenceCompletedEventHandler(ByVal sender As Object, ByVal e As getLanguagePreferenceCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class getLanguagePreferenceCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As String
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),String)
            End Get
        End Property
    End Class
End Namespace
