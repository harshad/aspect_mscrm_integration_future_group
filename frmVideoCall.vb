﻿Public Class frmVideoCall
    Private Sub frmVideoCall_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        wmpADV.uiMode = "none"
        btnConnect.Visible = True
        btnConnect2.Visible = False
        btnEndCall.Visible = False
        btnEndCall2.Visible = False

    End Sub

    Private Sub btnConnect_Click(sender As Object, e As EventArgs) Handles btnConnect.Click
        Try
            btnConnect.Visible = False
            btnConnect2.Visible = True
            btnEndCall.Visible = True
            btnEndCall2.Visible = False
        Catch ex As Exception
            'Do Nothing
        End Try
    End Sub

    Private Sub btnEndCall_Click(sender As Object, e As EventArgs) Handles btnEndCall.Click
        Try
            btnConnect.Visible = True
            btnConnect2.Visible = False
            btnEndCall.Visible = False
            btnEndCall2.Visible = True
        Catch ex As Exception
            'Do Nothing
        End Try
    End Sub
End Class
